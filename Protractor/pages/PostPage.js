var PostPage = function (){

    var that = this;

    this.postTitle = element(by.css("div.container-box > div > h1"));
    this.postDescription = element(by.css("div.container-box > div > p"));
    this.editButton = element(by.css(".glyphicon.glyphicon-pencil"));
    this.deleteButton = element(by.css(".glyphicon.glyphicon-remove"));
    this.postTitleField = element(by.id("id_title"));
    this.postDescriptionField = element(by.id("id_text"));
    this.saveButton = element(by.css("[type='submit']"));
}
  
  module.exports = PostPage;