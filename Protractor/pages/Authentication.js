var Authentication = function (){

  var that = this;

  this.loginButton = element(by.css("#content > div > ul > li:nth-child(2) > a"));
  this.loginModalText = element(by.css("h4.modal-title"));
  this.usernameField = element(by.id("id_username"));
  this.passwordField = element(by.id("id_password"));
  this.submitButton = element(by.css("[type='submit']"));

  this.loginUser = function(username, password)
  {
  	this.loginButton.click();

  	browser.wait(EC.elementToBeClickable(that.usernameField), 10000).then(function(){
  	  that.usernameField.clear();
	    that.usernameField.sendKeys(username);

	    that.passwordField.clear();
	    that.passwordField.sendKeys(password);
	  });

	  browser.wait(EC.elementToBeClickable(that.submitButton), 10000).then(function(){
      that.submitButton.click();
  	});
  };
}

module.exports = Authentication;