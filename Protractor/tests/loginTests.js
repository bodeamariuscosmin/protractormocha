var MainPage = require("./../pages/MainPage.js"),
  Authentication = require("./../pages/Authentication.js"),
  PostPage = require("./../pages/PostPage.js"),
  requestsFactory = require("../../Mocha/functions/RequestsFactory.js"),
  postId, postTitle, postDescription, editedPostTitle;

fdescribe("Log In Tests", function() {
  var requests = new requestsFactory(),
      mainPage = new MainPage(),
      postPage = new PostPage(),
      authentication = new Authentication();

  describe("Successful login test", function() {
    editedPostTitle = "Edited Post";

    // Merge dar am comentat
    // it("Should login and create post via API", function(done) {
    //   // Log in
    //   response = requests.loginUser('testaccount', 'password123');
    //   console.log(response);
    //   expect(response.username).toEqual('testaccount');

    //   // Create a post
    //   response = requests.createPost("Async test", "This is a Mocha test made using sync-request library!");
    //   console.log(response[0]);
    //   expect(response[0].title).toEqual("Async test");
    //   postId = response[0].id;
    //   postTitle = response[0].title;
    //   postDescription = response[0].text;
    //   done();
    // });

    it("Should open the site", function() {
      browser.get(browser.baseUrl);
    });

    it("Should verify the title of the site", function() {
      expect(browser.getTitle()).toContain("BMC - Homepage");
    });

    it("Should log in", function() {
      authentication.loginUser('testaccount', 'password123');
    });

    it("Should verify if the user is logged in", function() {
      browser.wait(EC.visibilityOf(mainPage.loggedInUserText), 10000).then(function(){
        expect(mainPage.loggedInUserText.getText()).toContain("testaccount");
      });
    });

    it("Should navigate to the post created via API", function() {
      browser.get(browser.baseUrl + '/post/' + postId);
      // browser.sleep(9000);
    });

    it("Should verify if the post title is correct", function() {
      browser.wait(EC.visibilityOf(postPage.postTitle), 10000).then(function(){
        expect(postPage.postTitle.getText()).toEqual(postTitle);
      });
    });

    it("Should verify if the post description is correct", function() {
      browser.wait(EC.visibilityOf(postPage.postDescription), 10000).then(function(){
        expect(postPage.postDescription.getText()).toEqual(postDescription);
      });
    });

    it("Should click the edit button", function() {
      browser.wait(EC.elementToBeClickable(postPage.editButton), 10000).then(function(){
        postPage.editButton.click();
      });
    });

    it("Should edit the title of the post", function() {
      browser.wait(EC.elementToBeClickable(postPage.postTitleField), 10000).then(function(){
        postPage.postTitleField.clear();
        postPage.postTitleField.sendKeys(editedPostTitle);
        postPage.saveButton.click();
      });
    });

    it("Should verify if the edited post title is correct", function() {
      browser.wait(EC.visibilityOf(postPage.postTitle), 10000).then(function(){
        expect(postPage.postTitle.getText()).toEqual(editedPostTitle);
      });
    });
  });
})
