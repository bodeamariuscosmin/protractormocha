'use strict'

var requestsFactory = require("../../Mocha/functions/RequestsFactory.js");

describe("Title Tests", function() {
  var requests = new requestsFactory(),
  response, user, post, body;

  it("create user", function() {
    user = requests.createUser();
    console.log(user.body);
    // mochaExpect("abc").toContain('a');
  });

  it("Should open the site", function() {
    console.log(user.body.username);
    post = requests.createPost("Mocha Test", "This is a Mocha test.");
    console.log(user.body);
  });

  it("login user", function(done) {
    response = requests.loginUser('testaccount', 'password123');
    body = response;
    console.log(body);
    expect(body.username).toEqual('testaccount');

    response = requests.createPost("Async test", "This is a Mocha test made using sync-request library!");
    body = response[0];
    console.log(body);
    expect(body.title).toEqual("Async test");
    done();
  });
})