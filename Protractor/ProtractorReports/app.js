var app = angular.module('reportingApp', []);

app.controller('ScreenshotReportController', function ($scope) {
    $scope.searchSettings = Object.assign({
        description: '',
        allselected: true,
        passed: true,
        failed: true,
        pending: true,
        withLog: true
    }, {}); // enable customisation of search settings on first page hit

    var initialColumnSettings = undefined; // enable customisation of visible columns on first page hit
    if (initialColumnSettings) {
        if (initialColumnSettings.displayTime !== undefined) {
            // initial settings have be inverted because the html bindings are inverted (e.g. !ctrl.displayTime)
            this.displayTime = !initialColumnSettings.displayTime;
        }
        if (initialColumnSettings.displayBrowser !== undefined) {
            this.displayBrowser = !initialColumnSettings.displayBrowser; // same as above
        }
        if (initialColumnSettings.displaySessionId !== undefined) {
            this.displaySessionId = !initialColumnSettings.displaySessionId; // same as above
        }
        if (initialColumnSettings.displayOS !== undefined) {
            this.displayOS = !initialColumnSettings.displayOS; // same as above
        }
        if (initialColumnSettings.inlineScreenshots !== undefined) {
            this.inlineScreenshots = initialColumnSettings.inlineScreenshots; // this setting does not have to be inverted
        }

    }


    $scope.inlineScreenshots = false;
    this.showSmartStackTraceHighlight = true;

    this.chooseAllTypes = function () {
        var value = true;
        $scope.searchSettings.allselected = !$scope.searchSettings.allselected;
        if (!$scope.searchSettings.allselected) {
            value = false;
        }

        $scope.searchSettings.passed = value;
        $scope.searchSettings.failed = value;
        $scope.searchSettings.pending = value;
        $scope.searchSettings.withLog = value;
    };

    this.isValueAnArray = function (val) {
        return isValueAnArray(val);
    };

    this.getParent = function (str) {
        var arr = str.split('|');
        str = "";
        for (var i = arr.length - 2; i > 0; i--) {
            str += arr[i] + " > ";
        }
        return str.slice(0, -3);
    };

    this.getSpec = function (str) {
        return getSpec(str);
    };


    this.getShortDescription = function (str) {
        return str.split('|')[0];
    };

    this.convertTimestamp = function (timestamp) {
        var d = new Date(timestamp),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh === 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    };


    this.round = function (number, roundVal) {
        return (parseFloat(number) / 1000).toFixed(roundVal);
    };


    this.passCount = function () {
        var passCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.passed) {
                passCount++;
            }
        }
        return passCount;
    };


    this.pendingCount = function () {
        var pendingCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.pending) {
                pendingCount++;
            }
        }
        return pendingCount;
    };


    this.failCount = function () {
        var failCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (!result.passed && !result.pending) {
                failCount++;
            }
        }
        return failCount;
    };

    this.passPerc = function () {
        return (this.passCount() / this.totalCount()) * 100;
    };
    this.pendingPerc = function () {
        return (this.pendingCount() / this.totalCount()) * 100;
    };
    this.failPerc = function () {
        return (this.failCount() / this.totalCount()) * 100;
    };
    this.totalCount = function () {
        return this.passCount() + this.failCount() + this.pendingCount();
    };

    this.applySmartHighlight = function (line) {
        if (this.showSmartStackTraceHighlight) {
            if (line.indexOf('node_modules') > -1) {
                return 'greyout';
            }
            if (line.indexOf('  at ') === -1) {
                return '';
            }

            return 'highlight';
        }
        return true;
    };


    var results = [
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007300dd-00bd-0082-00a3-001f00e200f6.png",
        "timestamp": 1540039517198,
        "duration": 227
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00de00d7-007e-001a-0094-00d30054007e.png",
        "timestamp": 1540039517923,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00580049-0011-0095-0042-00e700ea0063.png",
        "timestamp": 1540039518266,
        "duration": 2436
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540039520832,
                "type": ""
            }
        ],
        "screenShotFile": "00ce00d9-0017-00ce-005f-008000d30036.png",
        "timestamp": 1540039522399,
        "duration": 23
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540039524398,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540039524534,
                "type": ""
            }
        ],
        "screenShotFile": "002300b7-00aa-008f-00d3-001e008d00b0.png",
        "timestamp": 1540039523949,
        "duration": 1608
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0329703cb87b9b64d6d4c73b5dba23d5",
        "instanceId": 30509,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00dd00ee-00ea-0081-0015-0071004a002c.png",
        "timestamp": 1540039527169,
        "duration": 86
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f10080-00a5-00ae-0048-001f001600b6.png",
        "timestamp": 1540040924291,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00580056-00e7-00ce-0079-001f00420043.png",
        "timestamp": 1540040925106,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007c009f-0025-00c7-009f-00b0009100f3.png",
        "timestamp": 1540040925444,
        "duration": 2560
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540040928153,
                "type": ""
            }
        ],
        "screenShotFile": "008d0090-0032-0040-0060-003300a5008a.png",
        "timestamp": 1540040929646,
        "duration": 27
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540040931595,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540040931744,
                "type": ""
            }
        ],
        "screenShotFile": "003a00e4-000a-009c-00e1-004b00420043.png",
        "timestamp": 1540040931146,
        "duration": 1679
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0299282ae82943be5d1651eaf64f9e9f",
        "instanceId": 1378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00810072-00b9-00a1-004d-000b00de0068.png",
        "timestamp": 1540040934376,
        "duration": 91
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d900f4-009a-0098-004f-00ee00b700c9.png",
        "timestamp": 1540041359491,
        "duration": 235
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00000036-0092-0004-001a-00ff004900e7.png",
        "timestamp": 1540041360273,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005e00a8-00e5-005f-00cb-0051008c009d.png",
        "timestamp": 1540041360632,
        "duration": 1
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a9008e-0008-0031-003b-003e00e40024.png",
        "timestamp": 1540041360639,
        "duration": 1
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00800022-00e5-008a-00b3-000100f70003.png",
        "timestamp": 1540041360648,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1df892b447d34daa195c36cf1712144d",
        "instanceId": 1479,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007700f0-0075-0093-0021-0002001a0058.png",
        "timestamp": 1540041360655,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: done is not defined"
        ],
        "trace": [
            "ReferenceError: done is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:10:33)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"create user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:9:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "006800d5-00b8-00f6-006e-008200e7005c.png",
        "timestamp": 1540041471557,
        "duration": 230
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:16:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:15:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "002900bb-00bd-0071-0027-00eb00c9006b.png",
        "timestamp": 1540041472334,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0086006c-0070-00a8-0072-009800250075.png",
        "timestamp": 1540041472714,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007a009b-0087-0013-00b0-009d00d800ac.png",
        "timestamp": 1540041472722,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003400b1-0028-00ed-0011-00e1007a001a.png",
        "timestamp": 1540041472736,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f0b3741799d40b0302c63a2b827eb186",
        "instanceId": 1546,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f700b3-00be-00d5-00d2-004900ac00e5.png",
        "timestamp": 1540041472747,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b9001c-0089-0065-00f7-005b002400c1.png",
        "timestamp": 1540042090398,
        "duration": 2788
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042093327,
                "type": ""
            }
        ],
        "screenShotFile": "002700c2-00d7-00b8-00d9-00e100de002c.png",
        "timestamp": 1540042095084,
        "duration": 35
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540042097131,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042097277,
                "type": ""
            }
        ],
        "screenShotFile": "00540069-0015-003e-0026-007a0072007b.png",
        "timestamp": 1540042096679,
        "duration": 1659
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "009200bb-00a5-00a8-006c-00d100150058.png",
        "timestamp": 1540042099859,
        "duration": 87
    },
    {
        "description": "create user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fe00bb-00a4-00b2-00a8-00c000bc00d7.png",
        "timestamp": 1540042101430,
        "duration": 0
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a250c1d651a12991a0d1a728ef27d644",
        "instanceId": 1880,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ad00ef-003a-000c-00b7-00ae0021009a.png",
        "timestamp": 1540042101449,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests.createUser2 is not a function"
        ],
        "trace": [
            "TypeError: requests.createUser2 is not a function\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:10:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"create user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:9:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "000a00aa-00de-009f-00a4-00db006e000c.png",
        "timestamp": 1540042307162,
        "duration": 237
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:16:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:15:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "0011001f-0095-00ca-006f-00ae004b00c5.png",
        "timestamp": 1540042307931,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e200a3-0066-0068-009c-00fa000a0058.png",
        "timestamp": 1540042308288,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00450074-00a4-0062-0071-00e000a900ce.png",
        "timestamp": 1540042308295,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008b00a1-00ff-0009-00e0-006300270071.png",
        "timestamp": 1540042308304,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3ee097703820bf3b0e43974a257acecc",
        "instanceId": 1968,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00820097-0047-00cc-00bf-006b004e0016.png",
        "timestamp": 1540042308313,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests.loginUser is not a function"
        ],
        "trace": [
            "TypeError: requests.loginUser is not a function\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:10:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"create user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:9:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "009800cc-0029-0008-00fc-00c500ff00c5.png",
        "timestamp": 1540042420223,
        "duration": 221
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:16:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:15:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "003900ad-0005-0058-00ce-003a00af005c.png",
        "timestamp": 1540042420979,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e500f2-008a-0084-0036-00ef00520060.png",
        "timestamp": 1540042421331,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004f00a3-006f-008e-004f-0023003300e3.png",
        "timestamp": 1540042421339,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00cb00e5-0018-008a-00b0-00e8006b0036.png",
        "timestamp": 1540042421349,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a9f62263fe247b74c09afd2bfb043971",
        "instanceId": 2035,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dd007e-005e-00d6-005f-000700c700b2.png",
        "timestamp": 1540042421358,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b5005b-00b5-0039-0081-006100ec0067.png",
        "timestamp": 1540042456161,
        "duration": 237
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "008c0000-008c-001a-0093-0039007d0090.png",
        "timestamp": 1540042456955,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f00049-0041-0012-0036-00dc008000d3.png",
        "timestamp": 1540042457308,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d900ee-0039-0095-00a1-002800a300f6.png",
        "timestamp": 1540042457316,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b7005d-0088-0084-009b-00b2004500eb.png",
        "timestamp": 1540042457326,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e53d085336468fb465e2c797bd429c69",
        "instanceId": 2068,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b9005b-004a-0043-00f3-00b4005700ba.png",
        "timestamp": 1540042457335,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001000ab-0069-0045-0095-0010006a00a0.png",
        "timestamp": 1540042525009,
        "duration": 235
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00db00cd-00a7-0015-009e-00fe0009008b.png",
        "timestamp": 1540042525761,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e1009f-00ad-001d-0009-006100250076.png",
        "timestamp": 1540042526113,
        "duration": 2779
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042529046,
                "type": ""
            }
        ],
        "screenShotFile": "001600e1-005c-00c1-00b4-00d5001300f3.png",
        "timestamp": 1540042530529,
        "duration": 27
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540042532507,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042532679,
                "type": ""
            }
        ],
        "screenShotFile": "00c70002-0031-0027-007b-0023004d0000.png",
        "timestamp": 1540042532054,
        "duration": 1666
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74a0536696ce2f75581e369054258dfc",
        "instanceId": 2099,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "003c00be-00b4-00f4-0064-004500530014.png",
        "timestamp": 1540042535238,
        "duration": 87
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000900c4-00dc-00a4-009a-006c006000e1.png",
        "timestamp": 1540042586651,
        "duration": 227
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "008e002d-0068-00ef-0027-00f100b600de.png",
        "timestamp": 1540042587419,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001b0087-00da-00d7-00d9-00a4002100ad.png",
        "timestamp": 1540042587766,
        "duration": 2595
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042590504,
                "type": ""
            }
        ],
        "screenShotFile": "006600fc-00bc-00ec-0044-0098004400d4.png",
        "timestamp": 1540042591973,
        "duration": 24
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540042593913,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540042594061,
                "type": ""
            }
        ],
        "screenShotFile": "006d007a-00b6-009b-00f2-0017006400e1.png",
        "timestamp": 1540042593468,
        "duration": 1623
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "65100e93bc80b34660ec0fcd7ea9aff9",
        "instanceId": 2132,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00290087-0047-0016-00b9-007000b700df.png",
        "timestamp": 1540042596676,
        "duration": 103
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c100a3-0033-0095-00d2-00ad004400d5.png",
        "timestamp": 1540042962906,
        "duration": 234
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: baseUrl is not defined"
        ],
        "trace": [
            "ReferenceError: baseUrl is not defined\n    at requestsFactory.createPost (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Mocha/functions/requestsFactory.js:43:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:17:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:15:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00c400cb-00a1-0000-0053-00ea0035000e.png",
        "timestamp": 1540042963679,
        "duration": 3
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008500fa-0084-00c1-0041-005200140051.png",
        "timestamp": 1540042964037,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00180063-00b4-003c-0054-00e1002e000e.png",
        "timestamp": 1540042964045,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00750048-0009-00cf-00b4-003500070046.png",
        "timestamp": 1540042964054,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1bd0ec93a3af4e612ba4956b1c2c0c2f",
        "instanceId": 2302,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0036001a-004a-00b6-0086-00d9006900a9.png",
        "timestamp": 1540042964063,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004c00f1-0002-003b-007e-004e00b50051.png",
        "timestamp": 1540043006741,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: post is not defined"
        ],
        "trace": [
            "ReferenceError: post is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:17:10)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:15:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00b5006b-00ee-00c6-007a-00ab00a80084.png",
        "timestamp": 1540043007502,
        "duration": 3
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00670088-0034-00ad-005c-00ca00b30046.png",
        "timestamp": 1540043007871,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00990010-007d-0086-004d-00fe00970082.png",
        "timestamp": 1540043007882,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00280046-0032-00e9-00a7-004b009b000f.png",
        "timestamp": 1540043007893,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9f88d46107f2a164a1d72221c084b45",
        "instanceId": 2335,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a500ea-00a8-0060-002a-002500d000c3.png",
        "timestamp": 1540043007907,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00140001-0038-0011-005c-00b3009700fe.png",
        "timestamp": 1540043030639,
        "duration": 231
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d600f7-0098-00f0-0040-00ec00970004.png",
        "timestamp": 1540043031395,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b20087-00fa-004b-003d-00f0004000a0.png",
        "timestamp": 1540043031755,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008d0032-00ad-00e8-0076-00b000e100e7.png",
        "timestamp": 1540043031765,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d500cd-00c2-008b-0013-007f00ec006b.png",
        "timestamp": 1540043031775,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "29e6bef1b71a12b7b679a3a4086af130",
        "instanceId": 2367,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005d00bd-0037-00bf-00d3-005d00100063.png",
        "timestamp": 1540043031784,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "398717d40eb8abdc1ac3d18d7c9e6737",
        "instanceId": 2434,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000400f8-00f4-006f-00a2-00b000520017.png",
        "timestamp": 1540043197610,
        "duration": 230
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001f00c7-00cb-003b-00e8-00da009f005b.png",
        "timestamp": 1540043214047,
        "duration": 221
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000f000a-00fb-00c8-002d-005c003e00a8.png",
        "timestamp": 1540043214789,
        "duration": 68
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003000c8-0063-00d5-0064-006e0088003d.png",
        "timestamp": 1540043215217,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c60008-00f3-0055-000e-00e100a70043.png",
        "timestamp": 1540043215226,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00610084-0014-006e-005f-00a8004a0084.png",
        "timestamp": 1540043215235,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f5a04c4068d28c07c597a30288eefc04",
        "instanceId": 2464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003600da-000e-001c-0092-00c800f80039.png",
        "timestamp": 1540043215244,
        "duration": 0
    },
    {
        "description": "create user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "009800f0-0096-00a0-008a-00910027004a.png",
        "timestamp": 1540043296544,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00200013-0005-0053-004b-0042000b004f.png",
        "timestamp": 1540043297293,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0008006b-0034-0022-0010-005e001e00ad.png",
        "timestamp": 1540043297655,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00eb00cd-00df-001d-00d8-00a1006e00ec.png",
        "timestamp": 1540043297667,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dd00e7-0025-0093-007d-00b100b9000f.png",
        "timestamp": 1540043297679,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d628c743f4764820fcfd0a20e50029ee",
        "instanceId": 2503,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f900c2-00c7-0024-00d1-005b00f20014.png",
        "timestamp": 1540043297692,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006100b4-0029-00f3-0089-009500b90098.png",
        "timestamp": 1540047223963,
        "duration": 233
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:27:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00c000c0-0088-00d1-00a3-0059003b00d9.png",
        "timestamp": 1540047224730,
        "duration": 5
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00400036-006c-0073-00c5-00f800460030.png",
        "timestamp": 1540047225085,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ff0029-00f4-00d8-0014-004500c90016.png",
        "timestamp": 1540047225096,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002a005b-00d8-00d7-006f-003600630083.png",
        "timestamp": 1540047225106,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e56b1c0abaf633aa126193fd233657b2",
        "instanceId": 2659,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008d00de-00fb-00da-0018-00b5002900d3.png",
        "timestamp": 1540047225116,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d100fb-007e-0037-005c-00d9003f00e2.png",
        "timestamp": 1540047253314,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f40042-00bc-0011-00dd-00b1004f00cd.png",
        "timestamp": 1540047254066,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a90046-00ad-00ea-00ba-00b90018007f.png",
        "timestamp": 1540047254416,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00620046-00ca-0081-0077-00e200d5007c.png",
        "timestamp": 1540047254427,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005c0025-0072-009e-0065-0092000800bb.png",
        "timestamp": 1540047254436,
        "duration": 1
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "142c98cd7d2800c2389ebba43f19d6bc",
        "instanceId": 2692,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002a00c1-0027-006a-009e-00c6002f0024.png",
        "timestamp": 1540047254447,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002b00f5-00b9-0057-00e6-00e700a4003d.png",
        "timestamp": 1540047353425,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: done is not defined"
        ],
        "trace": [
            "ReferenceError: done is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:26:32)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00d200d2-00a8-002c-00d4-006e00a900d2.png",
        "timestamp": 1540047354184,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008e00a4-00d1-00eb-00a3-00dd00940030.png",
        "timestamp": 1540047354535,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "006c0034-0037-009d-0049-00ec004d009b.png",
        "timestamp": 1540047354547,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001b00d1-00d6-00e2-0036-000a00690039.png",
        "timestamp": 1540047354558,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "269711a4c295cbfd4f16e70f3eda46cc",
        "instanceId": 3255,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0010002b-00a8-00f1-0075-00f6001d0017.png",
        "timestamp": 1540047354568,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00780055-0015-008d-000e-00a3005b00d2.png",
        "timestamp": 1540047383618,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "008d00b7-00a2-000c-00df-00d800090045.png",
        "timestamp": 1540047384361,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002a00a2-000d-00c5-00b9-0082000000b3.png",
        "timestamp": 1540047384714,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f60081-0010-004c-005f-007d006200a2.png",
        "timestamp": 1540047384724,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00780070-005b-0078-0062-008d009d00d8.png",
        "timestamp": 1540047384734,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4861a74f9d1a560eb626b3a90645beec",
        "instanceId": 3296,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b7008a-0068-0046-004c-000300a10079.png",
        "timestamp": 1540047384744,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00650042-0016-00ce-0023-000400c6006b.png",
        "timestamp": 1540047483950,
        "duration": 232
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002a0059-0067-0046-00e3-009e008f008c.png",
        "timestamp": 1540047484708,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004a00e3-0000-0095-0092-002a00ee00d1.png",
        "timestamp": 1540047485061,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00580062-009e-0066-005d-00fa00010045.png",
        "timestamp": 1540047485071,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00660085-0096-0047-00e4-0060000e005a.png",
        "timestamp": 1540047485082,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "30f14121e89db688e295752010eccd43",
        "instanceId": 3348,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00980078-00e0-008e-00bb-000d00e800e1.png",
        "timestamp": 1540047485092,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a2009d-0027-0024-00e6-008200c4002c.png",
        "timestamp": 1540047494238,
        "duration": 231
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002a0042-00c6-0022-00e7-007e002d00a9.png",
        "timestamp": 1540047494977,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0036000c-003e-0029-00b3-008a002e0053.png",
        "timestamp": 1540047495335,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b00024-0051-00f3-003c-004d00490036.png",
        "timestamp": 1540047495347,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008f0081-0074-004c-0018-007000cf0000.png",
        "timestamp": 1540047495357,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "33312ab0f84c68804eae96b7fbfe1559",
        "instanceId": 3378,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d100cd-002c-00f8-0054-00af000000ca.png",
        "timestamp": 1540047495368,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001a00ef-0034-0036-00cc-009e00d00076.png",
        "timestamp": 1540047528696,
        "duration": 225
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a10032-008e-00cf-001d-008700f0008d.png",
        "timestamp": 1540047529448,
        "duration": 12
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a700c5-0046-00f4-0067-003100ec00fc.png",
        "timestamp": 1540047529818,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001a00d4-00a0-008e-0030-004b0054008f.png",
        "timestamp": 1540047529829,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000200e7-00bb-00ee-004b-0093004200a9.png",
        "timestamp": 1540047529841,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a34509107f5a640dd9037a37743df8a8",
        "instanceId": 3408,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00850072-00f5-0064-0037-008500f0002d.png",
        "timestamp": 1540047529852,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005600f2-0073-007b-001f-0087008c003f.png",
        "timestamp": 1540047655330,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ad00f4-0089-002d-00b8-002700ea0042.png",
        "timestamp": 1540047656118,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f00034-0072-0047-00cb-00ff005a00ae.png",
        "timestamp": 1540047656476,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ee0067-0036-00b8-0022-00e800320021.png",
        "timestamp": 1540047656487,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005000ac-00d2-00a7-0076-006c00b80021.png",
        "timestamp": 1540047656499,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3dbb86dddf1ed97eac86f242e830a797",
        "instanceId": 3464,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003100cb-00eb-00e7-00ec-006d00b4007e.png",
        "timestamp": 1540047656509,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00bf00a4-00b8-005c-007b-00d900300051.png",
        "timestamp": 1540047769692,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0072006d-00a6-0083-00bf-006c00950002.png",
        "timestamp": 1540047770446,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00240030-00e1-00cf-0048-00a400ba0018.png",
        "timestamp": 1540047770800,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00df00f4-003f-00f2-00c5-001c001c00cb.png",
        "timestamp": 1540047770811,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002b00f8-0065-004b-0042-005900d30057.png",
        "timestamp": 1540047770824,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "62f6b8871426ba55abe2c560f8976f29",
        "instanceId": 3512,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004600ca-00b6-000d-0052-0099007d00e1.png",
        "timestamp": 1540047770834,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a300e8-003d-0036-00d0-001e006300df.png",
        "timestamp": 1540047779919,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006e0092-005c-00fa-0098-00160042001b.png",
        "timestamp": 1540047780685,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00170039-00ea-00d9-002b-000b00e2009b.png",
        "timestamp": 1540047781048,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a60082-009b-0066-0059-0027007c0080.png",
        "timestamp": 1540047781058,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003b002f-008b-007b-0061-0057000a0068.png",
        "timestamp": 1540047781070,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "42e59d2baa748775b01f536de911790b",
        "instanceId": 3542,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00350092-00fd-0011-00c5-003700610094.png",
        "timestamp": 1540047781079,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0067001e-0075-0060-0008-001a00bc002d.png",
        "timestamp": 1540047807031,
        "duration": 238
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a100ff-0039-00f2-0029-006f00260075.png",
        "timestamp": 1540047807797,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b2006e-0058-008c-0098-0032009a005d.png",
        "timestamp": 1540047808162,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008f00c7-000b-00be-00ee-00400061003c.png",
        "timestamp": 1540047808174,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f800e5-005f-005e-00db-000b001e0044.png",
        "timestamp": 1540047808185,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8ccd4721e62237a58112cdbe2209c48e",
        "instanceId": 3574,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008900fc-00f8-00c0-0064-009800aa00a6.png",
        "timestamp": 1540047808194,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00bc0015-00ea-0045-00c4-00c500da0053.png",
        "timestamp": 1540047816881,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002b0081-00a8-0048-006e-009100f400f8.png",
        "timestamp": 1540047817652,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f30048-00b3-0005-0098-00cf00db0037.png",
        "timestamp": 1540047818009,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d60019-0065-00f0-00af-00b600300062.png",
        "timestamp": 1540047818021,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00610037-0056-007f-0012-0095009a0074.png",
        "timestamp": 1540047818033,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d621d8a859022a444723f9832fb1dc21",
        "instanceId": 3597,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00aa0016-0053-00c0-000c-008d005b000b.png",
        "timestamp": 1540047818042,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b0000e-008d-00a4-00ed-00d200a400b9.png",
        "timestamp": 1540047835932,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0072000e-00c6-0074-0028-003b002c006a.png",
        "timestamp": 1540047836693,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00cc00eb-00ee-009c-0005-009d009e006e.png",
        "timestamp": 1540047837068,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005c0015-00f4-00eb-0008-00de0078005d.png",
        "timestamp": 1540047837083,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0013004d-002a-0018-001d-00aa00db0080.png",
        "timestamp": 1540047837099,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "070de2319733690803f2b922ebf2969c",
        "instanceId": 3627,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007d004f-0076-0041-005d-00ec00a700b3.png",
        "timestamp": 1540047837112,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006d00e4-0067-00cd-00c0-000b0036001e.png",
        "timestamp": 1540047844549,
        "duration": 234
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00dc00ca-00ef-00cc-003f-000800f900e2.png",
        "timestamp": 1540047845317,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f80024-0012-0042-00e0-009100680041.png",
        "timestamp": 1540047845719,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00490034-005d-0022-00a3-00de000c0045.png",
        "timestamp": 1540047845736,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00050057-00f6-004c-00ba-002b00bf00a2.png",
        "timestamp": 1540047845754,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "fbcb43415c8ddd04a6b4b633d80d19f1",
        "instanceId": 3657,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b3007c-00cd-004b-00e8-00fd00da0041.png",
        "timestamp": 1540047845768,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00560095-00ef-003b-00bb-00e100740000.png",
        "timestamp": 1540047854814,
        "duration": 232
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ce00ac-005d-00ca-004b-00320098005c.png",
        "timestamp": 1540047855624,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001200d5-00d5-0005-0074-00d400cb003f.png",
        "timestamp": 1540047855984,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008c00d7-0007-001e-005e-006500be00ae.png",
        "timestamp": 1540047855996,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d9002b-00a0-008b-0058-00d200a100de.png",
        "timestamp": 1540047856011,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0d91db9bfe8a0a0aa1f6ffe084eab067",
        "instanceId": 3687,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008e00d9-00b8-00d3-0013-00ef00460015.png",
        "timestamp": 1540047856022,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ce00a9-00bb-00e8-0002-002b009100da.png",
        "timestamp": 1540047915991,
        "duration": 233
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a70056-0062-0031-00f6-00f5007a00af.png",
        "timestamp": 1540047916816,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002500f2-0035-0086-0083-000000f700b7.png",
        "timestamp": 1540047917172,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002c00d2-00b7-0071-0033-000300380057.png",
        "timestamp": 1540047917184,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fe005a-00f6-007d-0045-00a800f4006c.png",
        "timestamp": 1540047917199,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9975a2dd00f27389fe74b8293053be61",
        "instanceId": 3735,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00890055-004f-00f1-004c-004d006e0092.png",
        "timestamp": 1540047917213,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000600c8-00cf-00b3-00a3-006a00fd00ac.png",
        "timestamp": 1540048018840,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ba0045-00cb-0068-0079-000f0076007b.png",
        "timestamp": 1540048019627,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00730063-007d-00ad-00ab-00a0005000b0.png",
        "timestamp": 1540048019995,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009100ff-002b-0020-00bd-0087007a00d2.png",
        "timestamp": 1540048020008,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00df00fa-0039-0028-009b-0006008000c4.png",
        "timestamp": 1540048020021,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6c6eeeb474c8bfd3870dacdb7b949e7",
        "instanceId": 3801,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c40079-00a3-001f-0044-005900400022.png",
        "timestamp": 1540048020031,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f1bda97dd02e32082f3cbb2a21fde582",
        "instanceId": 3849,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00dc005f-0093-00da-0082-00d200680084.png",
        "timestamp": 1540048098047,
        "duration": 223
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000000b2-00ad-0083-00e2-003d007d008e.png",
        "timestamp": 1540048175057,
        "duration": 229
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:27:26)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00b40011-009f-00e1-004f-007a008c0090.png",
        "timestamp": 1540048175842,
        "duration": 4
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00970071-00f5-0007-0096-001900d200e5.png",
        "timestamp": 1540048176232,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000a005b-00e3-005d-0096-0021008100b8.png",
        "timestamp": 1540048176252,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0092004a-000f-000c-009c-00a1000f0023.png",
        "timestamp": 1540048176270,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b0d8062e3f4522577c62bc0a79d6bd7b",
        "instanceId": 3899,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0058000e-007b-0060-005d-00fa00d90081.png",
        "timestamp": 1540048176287,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006f00b3-00b2-008e-0051-009e00af0024.png",
        "timestamp": 1540048201424,
        "duration": 224
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e40066-00bf-0089-00de-00480024002d.png",
        "timestamp": 1540048202169,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f1007c-005a-0091-007d-00e9006900a0.png",
        "timestamp": 1540048202541,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ec00a2-000b-00d1-001a-0059009100c5.png",
        "timestamp": 1540048202554,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00790093-0087-008e-006e-0028006b0058.png",
        "timestamp": 1540048202568,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a4c56ea70c58687458e3efa0c772891c",
        "instanceId": 3929,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003b0096-00ed-0051-008f-001c00aa00cf.png",
        "timestamp": 1540048202580,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ff0097-00b8-0081-0090-00e700c00054.png",
        "timestamp": 1540048222467,
        "duration": 231
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c400ea-0004-007f-0019-00c900530076.png",
        "timestamp": 1540048223263,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: body is not defined"
        ],
        "trace": [
            "ReferenceError: body is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:32:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00fb0003-002b-0053-00b4-002200a70019.png",
        "timestamp": 1540048223652,
        "duration": 2
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0088006e-0063-00f0-002a-0035001f00fa.png",
        "timestamp": 1540048224011,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004b001b-00cc-0075-00dd-003c00390018.png",
        "timestamp": 1540048224028,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b100da-0064-00c3-002d-00fc00ff001d.png",
        "timestamp": 1540048224041,
        "duration": 1
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c2a6d2da84fb0fca05c320b2c16e444b",
        "instanceId": 3961,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000d00e5-006e-0055-005b-005e00190051.png",
        "timestamp": 1540048224059,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00760008-000c-0065-0091-005e002b00f9.png",
        "timestamp": 1540048239139,
        "duration": 225
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005100dd-00c0-0084-00f8-0038001d002d.png",
        "timestamp": 1540048239897,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'title' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'title' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:32:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "008600b1-000e-0087-0075-00ec00a700b8.png",
        "timestamp": 1540048240281,
        "duration": 6
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00800056-00b3-0047-000a-00b100b30012.png",
        "timestamp": 1540048240662,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bc0085-0056-0093-00cb-00ce008600aa.png",
        "timestamp": 1540048240676,
        "duration": 1
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002b00b8-00a6-0065-005a-002d000a0075.png",
        "timestamp": 1540048240695,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed08e2ec05ff84119c9bd15193306e68",
        "instanceId": 3991,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009500e6-00aa-00cf-00a5-007d009e0025.png",
        "timestamp": 1540048240711,
        "duration": 1
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e70038-004c-00d4-004b-0065004d0059.png",
        "timestamp": 1540048261039,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00020059-0031-0083-0016-002400590032.png",
        "timestamp": 1540048261816,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004100be-00de-000a-005a-00ef00c30053.png",
        "timestamp": 1540048262199,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007800ed-005f-00e8-0036-00a700cb005e.png",
        "timestamp": 1540048262573,
        "duration": 1
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00670049-0026-00e5-00b9-009d00c40038.png",
        "timestamp": 1540048262585,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f70072-00e7-0047-00e7-00ec00560084.png",
        "timestamp": 1540048262598,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0eb0f767709aebadb34e971ff15082e2",
        "instanceId": 4021,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "006a001e-00dd-009f-0006-00ea001a007a.png",
        "timestamp": 1540048262611,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests.createUser is not a function"
        ],
        "trace": [
            "TypeError: requests.createUser is not a function\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:22:14)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00c400ae-00ef-0085-00a2-003b00f100dc.png",
        "timestamp": 1540053878955,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0073001f-0091-00c5-00ac-002b004f00ea.png",
        "timestamp": 1540053879748,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e9004d-00fa-0063-008d-0088003e00e4.png",
        "timestamp": 1540053880104,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001600cd-00e0-004c-00ac-000e00b0002f.png",
        "timestamp": 1540053880453,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fb001a-003a-0048-00be-00b700ab00c4.png",
        "timestamp": 1540053880466,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009a00eb-00d9-0014-00cd-00bd00c90054.png",
        "timestamp": 1540053880482,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5491cf2c9563464c8502c81b53ac4640",
        "instanceId": 4576,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b300dd-00fc-00fa-0080-00af0034005b.png",
        "timestamp": 1540053880497,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e2008f-00d0-006f-008d-002900330080.png",
        "timestamp": 1540053907575,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "003200ce-00ba-0020-00a6-0032009d0042.png",
        "timestamp": 1540053908385,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0038007d-0033-00c9-006f-008b00c90037.png",
        "timestamp": 1540053908751,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00170083-0063-0034-0049-007e0017005a.png",
        "timestamp": 1540053909103,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008100f3-00bd-0079-0040-0016007000ae.png",
        "timestamp": 1540053909115,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000e0032-00d9-0002-00f6-005200e400be.png",
        "timestamp": 1540053909128,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c80abf3853f6ba9b9275651a1cebc2ba",
        "instanceId": 4610,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e400e0-00f0-00b6-0019-005d006f005e.png",
        "timestamp": 1540053909142,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00070076-004d-0081-00fd-004000f80044.png",
        "timestamp": 1540053941628,
        "duration": 233
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00dc000c-00bf-00ae-0096-0083002a00fa.png",
        "timestamp": 1540053942408,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d800e5-0009-0026-002e-00a600560024.png",
        "timestamp": 1540053942786,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0064008f-00ed-009a-00eb-002500cb0028.png",
        "timestamp": 1540053943135,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dc005c-0039-0058-0096-0081001000d4.png",
        "timestamp": 1540053943148,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bb00f1-00c2-00f2-0091-00b400e10073.png",
        "timestamp": 1540053943164,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6c24710130c55d27a506eb7c2c4864c7",
        "instanceId": 4656,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00880000-00bb-0030-00c4-009c00ad00f5.png",
        "timestamp": 1540053943191,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00bc00d9-0096-00f3-001a-00c200ce00f3.png",
        "timestamp": 1540124483371,
        "duration": 234
    },
    {
        "description": "encountered a declaration exception|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "TypeError: it.only is not a function"
        ],
        "trace": [
            "TypeError: it.only is not a function\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:6)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)\n    at Function.Module._load (module.js:497:3)"
        ],
        "browserLogs": [],
        "screenShotFile": "00b5001d-00eb-00cb-00c0-003100320086.png",
        "timestamp": 1540124484145,
        "duration": 3
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003f003e-00f1-0024-00a8-005a005900ce.png",
        "timestamp": 1540124484532,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00640025-00cd-00b9-00f4-007100f7004f.png",
        "timestamp": 1540124484551,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a00077-0066-00f0-001b-000d00ed0072.png",
        "timestamp": 1540124484566,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "77c7a2c86859e9dac20951b30d88872c",
        "instanceId": 8650,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dc0003-0023-0028-003f-0047000100b6.png",
        "timestamp": 1540124484581,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "7bcd6c19bd39b8f7d355d39f437bc27c",
        "instanceId": 8683,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00cf00dd-00d5-0040-00dc-00d700bb0063.png",
        "timestamp": 1540124506580,
        "duration": 222
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f40046-0010-00e6-001e-003d003c002e.png",
        "timestamp": 1540124527481,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "009700c9-0044-00b0-002f-0009001200d7.png",
        "timestamp": 1540124528206,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ee0009-009a-0003-00d9-00e500de0023.png",
        "timestamp": 1540124528577,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008a001c-0087-00ab-00f3-005f00960044.png",
        "timestamp": 1540124528939,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004c00cc-0099-00e1-00a2-00ce00aa00fe.png",
        "timestamp": 1540124528953,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d30022-006c-0023-0020-007d00c500f5.png",
        "timestamp": 1540124528970,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e9d21ff3dc2c399c29c0727509c8c7bc",
        "instanceId": 8717,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002c00de-0085-0017-0096-0055009c00c5.png",
        "timestamp": 1540124528997,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4d327237cfec38883ede23e0a2ddba45",
        "instanceId": 8748,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00370035-00bf-0092-0088-007f00d40096.png",
        "timestamp": 1540124545930,
        "duration": 227
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4d327237cfec38883ede23e0a2ddba45",
        "instanceId": 8748,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "0047000c-00c2-008d-00f3-00c9009c00f8.png",
        "timestamp": 1540124546663,
        "duration": 4
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b60053-0097-007e-00ab-003a00fe00f5.png",
        "timestamp": 1540124563593,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00e6006d-005c-00fe-000d-007a009600bd.png",
        "timestamp": 1540124564337,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b20052-0022-00ad-0083-00e4002000c8.png",
        "timestamp": 1540124564692,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008e00a8-0001-0028-0032-0079006b0030.png",
        "timestamp": 1540124565081,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d20057-009f-00db-0091-007000ec0035.png",
        "timestamp": 1540124565098,
        "duration": 1
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003700f0-003b-0097-0072-00db00cf00f7.png",
        "timestamp": 1540124565117,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "47fa98a586ceec8aa4aebf79685c868f",
        "instanceId": 8778,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f0008b-00c5-00fb-00d6-004f00530052.png",
        "timestamp": 1540124565145,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000c009c-0046-00a8-0038-005e00740071.png",
        "timestamp": 1540124598424,
        "duration": 229
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "003d003d-004a-009b-007a-00eb000f00b8.png",
        "timestamp": 1540124599178,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007900e0-000d-00e1-0049-004b005400dc.png",
        "timestamp": 1540124599564,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00be000b-0059-00d4-0029-00580051001b.png",
        "timestamp": 1540124599919,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001500b0-0049-008d-00ae-00e600770036.png",
        "timestamp": 1540124599935,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004e00c0-001f-00e8-00c5-00b700770010.png",
        "timestamp": 1540124599954,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "cd5ab0735de0d9abb86b072d4a9e5584",
        "instanceId": 8810,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008d000c-00ff-00a2-00fe-00cb00230061.png",
        "timestamp": 1540124599980,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00730052-00f2-00f0-0034-0077002500ec.png",
        "timestamp": 1540124624924,
        "duration": 227
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00710046-0000-003a-00fe-004900050049.png",
        "timestamp": 1540124625656,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005a00c8-00c1-0089-00a2-00d4004d0095.png",
        "timestamp": 1540124626018,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000b005f-001c-0055-0059-00bb00900023.png",
        "timestamp": 1540124626385,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c10080-006c-0013-00c4-0001005100cb.png",
        "timestamp": 1540124626402,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fb004f-0055-0054-0051-000e003200e3.png",
        "timestamp": 1540124626418,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "31335100afbf60ca0e609c43bc3237a8",
        "instanceId": 8836,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b80054-0090-00bb-006a-009c00380009.png",
        "timestamp": 1540124626446,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00700073-0017-00b0-001d-00d7007c005b.png",
        "timestamp": 1540124697363,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: postId is not defined"
        ],
        "trace": [
            "ReferenceError: postId is not defined\n    at requestsFactory.getAllPosts (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Mocha/functions/requestsFactory.js:27:43)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00fb0090-00d3-0043-00d3-0038000a00e9.png",
        "timestamp": 1540124698104,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f000bb-0006-00d3-001c-00e9001c00c2.png",
        "timestamp": 1540124698473,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00eb0029-00ff-00e0-000b-008e00820005.png",
        "timestamp": 1540124698840,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000100b0-005d-00c7-00d8-002d00670022.png",
        "timestamp": 1540124698856,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00280057-00b7-005f-003d-00d40034000e.png",
        "timestamp": 1540124698875,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "54296fadb851940c7e793e8da7d523d5",
        "instanceId": 8881,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007f00d1-0000-007b-0001-00c600e5006b.png",
        "timestamp": 1540124698906,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006e004f-004e-004b-00a9-0048007b00f4.png",
        "timestamp": 1540124710589,
        "duration": 230
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: postId is not defined"
        ],
        "trace": [
            "ReferenceError: postId is not defined\n    at requestsFactory.getAllPosts (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Mocha/functions/requestsFactory.js:27:43)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "000300d4-00ec-0031-00c1-000f00a90027.png",
        "timestamp": 1540124711345,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006e0026-002b-0013-0063-00c4008600ee.png",
        "timestamp": 1540124711717,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001a00f8-0093-00ee-0088-002600e30033.png",
        "timestamp": 1540124712072,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001d009c-00c6-001d-00d1-009b004c0092.png",
        "timestamp": 1540124712088,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000400b0-00c5-007f-00ac-009500b90069.png",
        "timestamp": 1540124712106,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a75c480d820e3a5372b2b6417fdc62f9",
        "instanceId": 8912,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002f00da-00db-00e3-0051-00af0001005d.png",
        "timestamp": 1540124712134,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0bf9ba9fff68a04faa2a8407758d7264",
        "instanceId": 8943,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00650049-008d-00a9-0014-00ce008c003c.png",
        "timestamp": 1540124733418,
        "duration": 234
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "24288345a66a9d7607f9f9e10e1c33df",
        "instanceId": 8973,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00280092-0037-001a-0021-002300e900e0.png",
        "timestamp": 1540124750959,
        "duration": 225
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "24288345a66a9d7607f9f9e10e1c33df",
        "instanceId": 8973,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000f006d-00dd-00bb-0079-002e007f0056.png",
        "timestamp": 1540124751688,
        "duration": 2
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "26c9406fa819ff51e24da9d75339f11d",
        "instanceId": 9003,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002c00ac-0012-00cd-00fc-0075005300cc.png",
        "timestamp": 1540124764794,
        "duration": 224
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "26c9406fa819ff51e24da9d75339f11d",
        "instanceId": 9003,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001300fb-00c8-0069-00f5-0041002d00c8.png",
        "timestamp": 1540124765521,
        "duration": 2
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001400fb-0078-00b5-001e-0050000300f7.png",
        "timestamp": 1540124786157,
        "duration": 217
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00db002b-00cc-001b-0075-00d6008200e9.png",
        "timestamp": 1540124786916,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00350025-00a5-00c1-0038-00a600c20038.png",
        "timestamp": 1540124787271,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000f00af-00b0-00e4-0081-001700c900db.png",
        "timestamp": 1540124787637,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0042009b-0017-0046-00be-00bb00aa003f.png",
        "timestamp": 1540124787654,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008f0089-007c-0023-00c7-006500b5004a.png",
        "timestamp": 1540124787672,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "e6cd480720d5f2375936cd7e31ff261a",
        "instanceId": 9033,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "006300d7-0011-00d0-0001-00170079001c.png",
        "timestamp": 1540124787702,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c800d2-00e6-009e-0069-000200b1004e.png",
        "timestamp": 1540124859602,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: response is not defined"
        ],
        "trace": [
            "ReferenceError: response is not defined\n    at requestsFactory.getAllPosts (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Mocha/functions/requestsFactory.js:29:8)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00d00022-007a-008c-0013-004800b8006d.png",
        "timestamp": 1540124860328,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004700a0-004c-0068-002d-006800dd00b6.png",
        "timestamp": 1540124860716,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "006c006c-0010-00d9-00d8-000100c9001e.png",
        "timestamp": 1540124861092,
        "duration": 2
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007800dd-0061-002b-001d-00c5006c00ca.png",
        "timestamp": 1540124861122,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d600a3-0073-0047-004c-0022008b00c4.png",
        "timestamp": 1540124861138,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "93b28952c871328c31e30a67f41073d0",
        "instanceId": 9065,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f10068-0028-007a-0091-007100530029.png",
        "timestamp": 1540124861155,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0060006f-0042-0017-006a-00b3008600ba.png",
        "timestamp": 1540124883295,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: response is not defined"
        ],
        "trace": [
            "ReferenceError: response is not defined\n    at requestsFactory.getAllPosts (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Mocha/functions/requestsFactory.js:29:8)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:21)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "000c003b-00e6-00df-001a-003d003a0060.png",
        "timestamp": 1540124884022,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f20050-0067-00a8-000e-003a009a00c0.png",
        "timestamp": 1540124884386,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00df0067-00e5-008c-00da-00b000710071.png",
        "timestamp": 1540124884752,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a8002c-00ea-00a1-00ab-003600a400db.png",
        "timestamp": 1540124884770,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fe001b-001b-00fc-00ca-008b00ca005b.png",
        "timestamp": 1540124884804,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b8bf7d9ba068899cb25e0a5019df279",
        "instanceId": 9102,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e100ea-0061-003a-0044-002900d80085.png",
        "timestamp": 1540124884821,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "009d005d-003e-00a6-00b1-009e00c30018.png",
        "timestamp": 1540125148306,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00710033-008b-00e8-007a-00f000c20039.png",
        "timestamp": 1540125149112,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "009800df-0050-00a8-003d-0030008b0052.png",
        "timestamp": 1540125149482,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009800a6-0020-00a3-00f7-002a001100c9.png",
        "timestamp": 1540125149835,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000000aa-00e3-0075-0035-000c00e70016.png",
        "timestamp": 1540125149853,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009c0047-00b2-009d-001a-0010008b00e5.png",
        "timestamp": 1540125149882,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c44b4efd5053505dbb1e8d2fb8182fac",
        "instanceId": 9162,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00980032-008b-0079-00f6-008700b80096.png",
        "timestamp": 1540125149898,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d100b1-00b1-0041-003d-003800cc001c.png",
        "timestamp": 1540125165883,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a10072-0095-0006-00cb-0022005b0037.png",
        "timestamp": 1540125166688,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b50031-00e4-009e-0033-00e8000400ac.png",
        "timestamp": 1540125167083,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00760011-0020-0062-00e5-0033001e00ba.png",
        "timestamp": 1540125167488,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004b0035-005d-00b0-00ec-00de00ae00b7.png",
        "timestamp": 1540125167508,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00090080-0092-00da-00e2-00d600940067.png",
        "timestamp": 1540125167538,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "eaed9a1263bee51170345f46a7a39772",
        "instanceId": 9193,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00770064-00a2-00d7-0074-00b400c400c8.png",
        "timestamp": 1540125167556,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00390012-0019-0067-005c-00e7006c0086.png",
        "timestamp": 1540125176821,
        "duration": 227
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "003700b1-0034-0065-0079-006b00bb008c.png",
        "timestamp": 1540125177617,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f30001-0012-0057-0021-00fb002b00b3.png",
        "timestamp": 1540125178010,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0055005f-00be-004e-00a2-00e500b600ae.png",
        "timestamp": 1540125178382,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c6000a-006b-0037-007f-000c00660064.png",
        "timestamp": 1540125178403,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00500044-0037-0014-00b1-007700cd00bb.png",
        "timestamp": 1540125178451,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d66ea15cd42e8f0ca79679bb637e73b3",
        "instanceId": 9223,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00df00d2-0061-0069-006d-00e100e000d9.png",
        "timestamp": 1540125178475,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000a0062-00dd-00d7-00eb-005f00d000fb.png",
        "timestamp": 1540125200618,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d10019-00b9-004f-00e8-002d0019007c.png",
        "timestamp": 1540125201392,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00140058-00a6-0090-0064-001300540020.png",
        "timestamp": 1540125201783,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009200dd-00b6-0083-00e4-006300ba00f6.png",
        "timestamp": 1540125202165,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007f001b-006e-0026-0044-00ef00cd0069.png",
        "timestamp": 1540125202191,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008f0041-00f1-000b-00de-002400670096.png",
        "timestamp": 1540125202237,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f98808f889aa6a671875ccb6f30b0b5d",
        "instanceId": 9253,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002500b8-00c7-00da-00e6-00ad000200da.png",
        "timestamp": 1540125202262,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00be00ef-007d-004b-00b9-009c00b50089.png",
        "timestamp": 1540125209433,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "009300b5-0090-00b4-008c-002500730021.png",
        "timestamp": 1540125210222,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00730002-0015-000f-00ec-005f002f0007.png",
        "timestamp": 1540125210613,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0052003d-002e-002d-002c-00ef004500e6.png",
        "timestamp": 1540125210987,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0068003f-005f-0035-0070-007900b9000f.png",
        "timestamp": 1540125211011,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00670044-00ff-00f1-0085-002300250069.png",
        "timestamp": 1540125211053,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "831b9c957971091d02078849a77a56e0",
        "instanceId": 9284,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00310085-004d-00bf-0013-00c1003f0066.png",
        "timestamp": 1540125211073,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b100a9-00be-007a-0013-006100430078.png",
        "timestamp": 1540125290501,
        "duration": 224
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c00019-00f8-001f-0038-009e004400c9.png",
        "timestamp": 1540125291281,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e000c0-0065-00ca-00bf-00b7009e00d4.png",
        "timestamp": 1540125291675,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00630004-0016-0030-00e9-0052003500a5.png",
        "timestamp": 1540125292023,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0004002b-000a-00a9-00c1-0069004d0009.png",
        "timestamp": 1540125292043,
        "duration": 1
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009800be-006a-0010-0036-006d002f00c5.png",
        "timestamp": 1540125292072,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "6134a7ed41c7538da8e399261df4ab6b",
        "instanceId": 9334,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00eb00f0-0017-00f7-00dd-0061004f00de.png",
        "timestamp": 1540125292088,
        "duration": 1
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002c00a9-007f-0069-0027-0004000300f0.png",
        "timestamp": 1540125303423,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00af00f1-0064-0000-0066-007f005400a2.png",
        "timestamp": 1540125304259,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00170021-009c-00f4-00cb-002b00380063.png",
        "timestamp": 1540125304656,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b800d7-00b1-004f-008c-000700d500bd.png",
        "timestamp": 1540125305030,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e00030-007b-0004-0085-006600b3004b.png",
        "timestamp": 1540125305056,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009e0006-00a4-003c-00e2-009500db0053.png",
        "timestamp": 1540125305096,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0c9f3e234b183b87b03d6ec21f3aef9c",
        "instanceId": 9364,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fe009a-00cf-009f-006e-005800cd0086.png",
        "timestamp": 1540125305120,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0097002e-00c5-00dd-001c-008400a700da.png",
        "timestamp": 1540125348224,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a5008e-00c5-00bb-0070-00560088004d.png",
        "timestamp": 1540125349016,
        "duration": 5
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005100b8-0094-00c9-008c-00fb00bf00d6.png",
        "timestamp": 1540125349398,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003200a6-00bd-0011-009c-000f00800086.png",
        "timestamp": 1540125349779,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e4000b-00bf-00b1-0021-00f300c300d3.png",
        "timestamp": 1540125349800,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007300e6-000f-0004-0037-004100cc00f2.png",
        "timestamp": 1540125349829,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9da1382d6c1a14fd0e8fddc893a55fff",
        "instanceId": 9483,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e40039-0069-00b0-0028-0065003700bd.png",
        "timestamp": 1540125349846,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002600a4-00c1-00ee-00c6-0081001200dc.png",
        "timestamp": 1540125363205,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:32:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "0001004a-00ef-00e8-0087-004900db00e7.png",
        "timestamp": 1540125363973,
        "duration": 9
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a8002a-00f9-0084-00dc-00c300a400e8.png",
        "timestamp": 1540125364357,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b800df-0084-00f0-0079-00fc00840020.png",
        "timestamp": 1540125364714,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00de00fe-00fd-00b3-00a9-00fe00df00c9.png",
        "timestamp": 1540125364734,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00030025-005d-0058-007a-00f100b1001f.png",
        "timestamp": 1540125364765,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b6dee2ff6c54806ec41835157e489e6e",
        "instanceId": 9514,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007d0000-008b-004c-0044-007b004b001c.png",
        "timestamp": 1540125364784,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0023005f-001d-0044-0092-000900dc0022.png",
        "timestamp": 1540125385138,
        "duration": 228
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:32:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00670010-003d-0093-005d-006d00ad00c9.png",
        "timestamp": 1540125385942,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00260022-0091-00ea-00bd-00860099007d.png",
        "timestamp": 1540125386319,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d100b4-00c0-00f8-0051-00c600d40059.png",
        "timestamp": 1540125386707,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00730045-00af-00a3-006c-0077004a00bf.png",
        "timestamp": 1540125386747,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0018007f-006f-0070-002b-0021004c0013.png",
        "timestamp": 1540125386773,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b9304d3c2a7acc4ece3fb01d04ba5958",
        "instanceId": 9548,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00970000-0028-00ea-004e-007700780045.png",
        "timestamp": 1540125386819,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0097009b-00cf-009e-00dc-00da00e700de.png",
        "timestamp": 1540125564349,
        "duration": 223
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:32:22)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00be00c3-0073-00e1-002c-005400160079.png",
        "timestamp": 1540125565156,
        "duration": 7
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0035002d-003e-0098-0073-002f00100045.png",
        "timestamp": 1540125565535,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e000b5-00b6-00ba-0001-00b900bd00c5.png",
        "timestamp": 1540125565911,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004c0034-00fc-002c-004f-00d300fe00ef.png",
        "timestamp": 1540125565953,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00320027-0002-008d-0007-00f500610057.png",
        "timestamp": 1540125565982,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "50bdbfcfca1772c81d83132dfbbb63cc",
        "instanceId": 9629,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005f00fc-00e7-0032-0040-00ab00360030.png",
        "timestamp": 1540125566029,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002900f1-0084-0033-00f0-00890054000a.png",
        "timestamp": 1540125583944,
        "duration": 225
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c200c2-0034-00ff-009a-009a0063004f.png",
        "timestamp": 1540125584738,
        "duration": 3
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d6001a-0051-001d-0079-002b007a00df.png",
        "timestamp": 1540125585123,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001700c5-00d5-00f8-00a5-004700ea00a6.png",
        "timestamp": 1540125585493,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00320062-005b-00f3-00a2-00ef00a0005f.png",
        "timestamp": 1540125585528,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000e0005-00d4-00c3-0039-00c0002a0082.png",
        "timestamp": 1540125585545,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "739463ed690fe43a539169e5cc65fa50",
        "instanceId": 9660,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001a00d8-009e-001f-009e-002e00c10067.png",
        "timestamp": 1540125585576,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001f00a2-0036-00ce-00ed-007b00ba00c2.png",
        "timestamp": 1540125660183,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "009d008b-008a-0046-0052-0061006c00e7.png",
        "timestamp": 1540125661000,
        "duration": 20
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00fb007e-00f2-0041-0076-000300ad00be.png",
        "timestamp": 1540125661406,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ab00cb-0039-0097-0079-00320016004c.png",
        "timestamp": 1540125661787,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007700e8-00b3-00ca-001a-00100067004c.png",
        "timestamp": 1540125661819,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00da00d9-006f-00fd-0070-00bd006c00bf.png",
        "timestamp": 1540125661837,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "24de96d88c4d8a3d74b43104b474d5df",
        "instanceId": 9695,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e10027-0003-003c-0080-0003002100f0.png",
        "timestamp": 1540125661868,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0070003e-00f3-0071-003c-00f800750027.png",
        "timestamp": 1540125680896,
        "duration": 232
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004300c0-000e-00c7-00f6-00c0005900a6.png",
        "timestamp": 1540125681721,
        "duration": 20
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00110063-00a2-00dd-00fa-006d00ea007b.png",
        "timestamp": 1540125682115,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005c00d8-0034-00d6-00e2-00780045007e.png",
        "timestamp": 1540125682496,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f200b2-00c4-002b-0004-00a0004f00e3.png",
        "timestamp": 1540125682527,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005900a1-002f-00a4-00db-00ba00770040.png",
        "timestamp": 1540125682546,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "5927ee29ddf888e62a9da46f4771f325",
        "instanceId": 9725,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e9005c-00b3-008e-00a0-00ef0084003c.png",
        "timestamp": 1540125682577,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007500da-00bb-005b-0099-007c00230024.png",
        "timestamp": 1540125709400,
        "duration": 234
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001e009c-00b4-005f-0015-008600200085.png",
        "timestamp": 1540125710230,
        "duration": 41
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b10034-009a-0058-00c1-000e00c2005e.png",
        "timestamp": 1540125710651,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ae005e-004b-0000-000d-00110007004d.png",
        "timestamp": 1540125711040,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ae0079-00c3-002a-006d-00d900c40004.png",
        "timestamp": 1540125711092,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00290034-0078-002b-008a-002a00f200d7.png",
        "timestamp": 1540125711120,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "46685ac2414b66bfa1627f63cebbf9a9",
        "instanceId": 9758,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0079002c-0036-001e-00a3-00ac009e00a9.png",
        "timestamp": 1540125711164,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ed00ec-007b-00ef-002f-00c400a500de.png",
        "timestamp": 1540125727468,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001e00ea-002e-0015-0093-003c002d00fb.png",
        "timestamp": 1540125728250,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001c0044-002a-000f-00ba-003c00aa00d8.png",
        "timestamp": 1540125728644,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b50046-0031-003e-00f4-0098004500c7.png",
        "timestamp": 1540125729021,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fd00ed-0003-002b-004e-004400110029.png",
        "timestamp": 1540125729053,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c80084-0097-00cd-0048-00f400b700f7.png",
        "timestamp": 1540125729071,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "018d88a7b69d5ac0b970bdc536d0bfe8",
        "instanceId": 9789,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00230069-00ef-00b5-003d-00fb00bd003c.png",
        "timestamp": 1540125729103,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a40039-00f2-0063-003f-00ac00b70031.png",
        "timestamp": 1540125758796,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00f700b8-00a9-0069-0004-003f00110019.png",
        "timestamp": 1540125759545,
        "duration": 6
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00640007-00e7-002e-00aa-00bb007e0018.png",
        "timestamp": 1540125759938,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e500cc-009f-00fd-0008-006200400058.png",
        "timestamp": 1540125760297,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00340023-0000-000e-001e-00a800f5000e.png",
        "timestamp": 1540125760329,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0070002a-00f0-0002-000d-007400f40009.png",
        "timestamp": 1540125760349,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c835fb3bfaf8efbc1482094b02dab9c9",
        "instanceId": 9819,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a2000d-0076-00da-0006-00f900960046.png",
        "timestamp": 1540125760381,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "77eba56daa051bed5fcc164a27da9a5d",
        "instanceId": 9865,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00dc00f3-0062-006b-00e7-006700c1004f.png",
        "timestamp": 1540125814037,
        "duration": 237
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "003f0012-0065-0040-006a-002300f200dd.png",
        "timestamp": 1540126215879,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c4000b-0010-00b7-00d4-008e008600ad.png",
        "timestamp": 1540126216701,
        "duration": 7
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b2003d-00b8-0079-0095-001c00320090.png",
        "timestamp": 1540126217087,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d90055-00fb-00f9-0027-00d400d300ce.png",
        "timestamp": 1540126217445,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008200af-0092-00cb-00c9-0070009900a6.png",
        "timestamp": 1540126217481,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00de0004-0056-001c-00e7-003000870043.png",
        "timestamp": 1540126217501,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d65ca5656a16939cc9f881c4fc33f7db",
        "instanceId": 9945,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bf0031-0004-0015-0079-00b500080085.png",
        "timestamp": 1540126217534,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "001f003a-000a-0009-0024-00b80067003e.png",
        "timestamp": 1540126229613,
        "duration": 226
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002d00c5-00e6-00a8-0072-008700b700ce.png",
        "timestamp": 1540126230427,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000f00db-0078-000e-00d8-0067007000aa.png",
        "timestamp": 1540126230798,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e30075-00c9-0032-0018-008a00c300b4.png",
        "timestamp": 1540126231159,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bb0020-00f0-002f-00a0-0092004000b8.png",
        "timestamp": 1540126231195,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001d00e7-00a5-003c-00aa-009100c40031.png",
        "timestamp": 1540126231215,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "8a4cfa8a25afdd4a0f8fd80fd14a8215",
        "instanceId": 9977,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a80099-00ce-0014-00b6-008500b70080.png",
        "timestamp": 1540126231247,
        "duration": 1
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a6d5b43f501da017669624610aeaa729",
        "instanceId": 10010,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0009001a-00eb-0043-0096-00c7004400be.png",
        "timestamp": 1540126249796,
        "duration": 222
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004e0091-001b-0066-004d-00370025000a.png",
        "timestamp": 1540126274722,
        "duration": 231
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: done is not defined"
        ],
        "trace": [
            "ReferenceError: done is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:33)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "007f0069-00bd-0056-00b0-0048008000aa.png",
        "timestamp": 1540126275542,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0071005f-0040-00e3-00fd-00aa00bd000d.png",
        "timestamp": 1540126275920,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0069000b-0013-00f9-00df-000000e800b0.png",
        "timestamp": 1540126276289,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005c00ad-006a-0000-0043-005900fd00d6.png",
        "timestamp": 1540126276311,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001b00e0-0075-0029-00f0-008100e000a2.png",
        "timestamp": 1540126276357,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "61f157b5065d9a36f67e41ffd679d1f9",
        "instanceId": 10050,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00770043-0095-0008-0018-00d500fc007f.png",
        "timestamp": 1540126276380,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "74523a011b7cf4dd53cb4fa459124d47",
        "instanceId": 10174,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00b500a4-00ef-0097-008e-00c2009100b0.png",
        "timestamp": 1540126299927,
        "duration": 222
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00d40078-001b-00e3-000e-00d5007a0049.png",
        "timestamp": 1540126365617,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00800085-0099-0032-00ca-00c000c400a4.png",
        "timestamp": 1540126366375,
        "duration": 4
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005500f0-0089-0037-00e3-00dc0083003d.png",
        "timestamp": 1540126366760,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00950059-001f-00a3-00c0-000400ee0081.png",
        "timestamp": 1540126367122,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0027001c-00ba-0041-0067-009900b40019.png",
        "timestamp": 1540126367147,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001200e5-0060-0020-00f0-0004002600ad.png",
        "timestamp": 1540126367181,
        "duration": 1
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "b59bd9e5ef722ce174f880cc3b52e966",
        "instanceId": 10227,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008400d2-00ab-0016-0059-00a200420029.png",
        "timestamp": 1540126367201,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00be0034-0005-00b4-008d-00b7006000fd.png",
        "timestamp": 1540126486335,
        "duration": 222
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Unexpected token u in JSON at position 0"
        ],
        "trace": [
            "SyntaxError: Unexpected token u in JSON at position 0\n    at JSON.parse (<anonymous>)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:17)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "009300a6-0028-0090-00c1-0028008100e7.png",
        "timestamp": 1540126487109,
        "duration": 5
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00c300f7-000d-004d-0089-002500810010.png",
        "timestamp": 1540126487494,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00d30084-0073-005e-0073-00c0009d0075.png",
        "timestamp": 1540126487881,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00fb001a-0009-001d-0084-004e001f005b.png",
        "timestamp": 1540126487904,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "002f0080-00c1-005f-0013-003c00f400e7.png",
        "timestamp": 1540126487937,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "878d911bb4e54cd069c05227d3fb1c60",
        "instanceId": 10282,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00080020-001e-0074-0056-00bb0026008c.png",
        "timestamp": 1540126487957,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "db2b6d1d20f0e3cd78245ebf0e988d9d",
        "instanceId": 10328,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0059009f-00f4-009d-00f3-00d50080003c.png",
        "timestamp": 1540126605026,
        "duration": 227
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b2dce84e5a71956904ef470622bca518",
        "instanceId": 10359,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "008a00fe-00c9-00b2-0095-0077005c005d.png",
        "timestamp": 1540126620560,
        "duration": 223
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006d00cd-008d-0089-0058-009c005f0015.png",
        "timestamp": 1540126781352,
        "duration": 220
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'body' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'body' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:31:28)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should open the site\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00b50050-0012-00e3-00a9-0042000c00de.png",
        "timestamp": 1540126782199,
        "duration": 5
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f000ee-005b-00f4-004d-00a100080031.png",
        "timestamp": 1540126782570,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008c00dc-0075-0002-003f-00a2007d004c.png",
        "timestamp": 1540126782942,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f300b7-0072-00c6-00ad-00f100650098.png",
        "timestamp": 1540126782964,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00330013-00dc-001b-0008-00fd0050009e.png",
        "timestamp": 1540126782999,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c5c541e51c7c3bb78326e33ebb8d8fe0",
        "instanceId": 10397,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "006c0000-0023-00e4-005d-00d400f80083.png",
        "timestamp": 1540126783020,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: response is not defined"
        ],
        "trace": [
            "ReferenceError: response is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:24:5)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00050005-009e-003e-0073-00f2008000e5.png",
        "timestamp": 1540146058023,
        "duration": 591
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00bf008e-00f6-00a4-0093-009600470037.png",
        "timestamp": 1540146059252,
        "duration": 2
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "002d0026-0059-002a-008f-00a5005e003e.png",
        "timestamp": 1540146059635,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c30068-0047-00b5-004b-00f200bf0074.png",
        "timestamp": 1540146060016,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bc008b-0007-000c-00f1-006b00050026.png",
        "timestamp": 1540146060040,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004b006e-0059-00ea-0012-00c9005c00a0.png",
        "timestamp": 1540146060078,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "3b55df32b087a0a474897986b12cff8a",
        "instanceId": 12279,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00630072-0062-0077-0089-006f00a200d6.png",
        "timestamp": 1540146060101,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: response is not defined"
        ],
        "trace": [
            "ReferenceError: response is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:23:14)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00980061-00d8-00f8-0047-00d700480083.png",
        "timestamp": 1540146091270,
        "duration": 536
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0067003a-00c8-0071-0008-00e2008d00fa.png",
        "timestamp": 1540146092417,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00050059-0054-00a2-003b-00290061008b.png",
        "timestamp": 1540146092827,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dd0014-00c4-0090-00e5-00b7004c0064.png",
        "timestamp": 1540146093209,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00840076-00b2-0062-00dc-003f008f003c.png",
        "timestamp": 1540146093233,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a6005c-008b-00fb-00d7-009000520046.png",
        "timestamp": 1540146093274,
        "duration": 1
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "f9e47652bc8923d650c70768e82cec38",
        "instanceId": 12324,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a800d8-00f1-00bc-0093-00d100d400b9.png",
        "timestamp": 1540146093300,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "0099005e-00d7-0055-004e-0053006e008f.png",
        "timestamp": 1540146183066,
        "duration": 547
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00f600dd-0028-004f-00e3-00a100370082.png",
        "timestamp": 1540146184139,
        "duration": 1
    },
    {
        "description": "Should open the site|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a500c7-0093-00ec-00e9-00ef00cb0058.png",
        "timestamp": 1540146184545,
        "duration": 1
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001c0010-0031-00d5-005d-002300f800c2.png",
        "timestamp": 1540146184948,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0080002b-0090-00de-0021-00080004002d.png",
        "timestamp": 1540146184978,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009100a9-00a1-00c0-00dd-00b1001100bb.png",
        "timestamp": 1540146185023,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "1a51dd892e38b72ed6644f30cbb21ca0",
        "instanceId": 12410,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e100b5-006c-00c9-0025-00aa00d400a1.png",
        "timestamp": 1540146185049,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "0ea292e3613a2313df1e6823491d1c77",
        "instanceId": 13133,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: fact is not defined"
        ],
        "trace": [
            "ReferenceError: fact is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:22:5)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "004d00b2-005f-00a5-00f6-004e00760016.png",
        "timestamp": 1540147819694,
        "duration": 221
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0ea292e3613a2313df1e6823491d1c77",
        "instanceId": 13133,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00be00d5-0029-00f8-00b0-00f2006e00a8.png",
        "timestamp": 1540147820470,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0ea292e3613a2313df1e6823491d1c77",
        "instanceId": 13133,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005a0056-0035-005f-004a-00ee005b0000.png",
        "timestamp": 1540147820499,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0ea292e3613a2313df1e6823491d1c77",
        "instanceId": 13133,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f200f3-00b9-0092-006b-00a600f600c7.png",
        "timestamp": 1540147820537,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "0ea292e3613a2313df1e6823491d1c77",
        "instanceId": 13133,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "008b0091-00ac-00f6-0034-009500e7006b.png",
        "timestamp": 1540147820563,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "ab4f732caaa6668edd657662c6250aed",
        "instanceId": 13169,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: response is not defined"
        ],
        "trace": [
            "ReferenceError: response is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:22:14)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "00790088-00ed-006e-00fd-00b0009400e3.png",
        "timestamp": 1540147838640,
        "duration": 696
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ab4f732caaa6668edd657662c6250aed",
        "instanceId": 13169,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b4008c-00bd-0034-00e3-00a700400016.png",
        "timestamp": 1540147839891,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ab4f732caaa6668edd657662c6250aed",
        "instanceId": 13169,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00a800cd-0087-0061-00e3-00600041009d.png",
        "timestamp": 1540147839921,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ab4f732caaa6668edd657662c6250aed",
        "instanceId": 13169,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "001e0079-000c-000c-0090-00e8006c009a.png",
        "timestamp": 1540147839964,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ab4f732caaa6668edd657662c6250aed",
        "instanceId": 13169,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000400bf-0029-00f0-008c-00d9001500f4.png",
        "timestamp": 1540147839993,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d3eccc517d37399929329514d91c0612",
        "instanceId": 13205,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'equal' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'equal' of undefined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:25:29)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"login user\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:21:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:5:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [],
        "screenShotFile": "001b0064-0079-005e-006f-00ba009c00f5.png",
        "timestamp": 1540147861281,
        "duration": 729
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d3eccc517d37399929329514d91c0612",
        "instanceId": 13205,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "009f00d3-00a1-00a7-0020-004100e400ce.png",
        "timestamp": 1540147862544,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d3eccc517d37399929329514d91c0612",
        "instanceId": 13205,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "005e000e-009a-0033-007e-002100f3001f.png",
        "timestamp": 1540147862573,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d3eccc517d37399929329514d91c0612",
        "instanceId": 13205,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000a00b5-003d-00f2-0031-00a6008e0021.png",
        "timestamp": 1540147862617,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d3eccc517d37399929329514d91c0612",
        "instanceId": 13205,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00090028-006d-0086-0051-001200d000c6.png",
        "timestamp": 1540147862645,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "ed9a645efd2c3fc67a13db66eb505ae6",
        "instanceId": 13241,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Expected 'Async test' to equal 'ceva'."
        ],
        "trace": [
            "Error: Failed expectation\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/titleTest.js:30:24)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>"
        ],
        "browserLogs": [],
        "screenShotFile": "007b00b3-0088-002d-00b1-00b300d7001b.png",
        "timestamp": 1540147890462,
        "duration": 1067
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed9a645efd2c3fc67a13db66eb505ae6",
        "instanceId": 13241,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003d0021-00af-002d-0048-0072004c003a.png",
        "timestamp": 1540147892069,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed9a645efd2c3fc67a13db66eb505ae6",
        "instanceId": 13241,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b2000b-0052-00f2-000a-00980078003a.png",
        "timestamp": 1540147892101,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed9a645efd2c3fc67a13db66eb505ae6",
        "instanceId": 13241,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00dd0021-0071-001d-00a1-005b00c000df.png",
        "timestamp": 1540147892148,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "ed9a645efd2c3fc67a13db66eb505ae6",
        "instanceId": 13241,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "004000aa-00e6-008d-0072-00bf00ec009e.png",
        "timestamp": 1540147892179,
        "duration": 0
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "727f52545c6edf22073127229b1a94f6",
        "instanceId": 13278,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "0047007f-00a5-00f2-000e-004800260037.png",
        "timestamp": 1540147908572,
        "duration": 1070
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "727f52545c6edf22073127229b1a94f6",
        "instanceId": 13278,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007b0000-0040-0070-0013-00ad00df0014.png",
        "timestamp": 1540147910183,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "727f52545c6edf22073127229b1a94f6",
        "instanceId": 13278,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ac00b9-00e8-005a-00d6-009a00360089.png",
        "timestamp": 1540147910218,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "727f52545c6edf22073127229b1a94f6",
        "instanceId": 13278,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00f10067-00a7-0053-0045-003000590078.png",
        "timestamp": 1540147910265,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "727f52545c6edf22073127229b1a94f6",
        "instanceId": 13278,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0077001f-00dd-00f9-0014-0054002800f2.png",
        "timestamp": 1540147910293,
        "duration": 0
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9fbb05c731caf23b0fd58f3fbc1bb87a",
        "instanceId": 15329,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "000300e4-0077-0043-00bd-00310075004a.png",
        "timestamp": 1540190431137,
        "duration": 5398
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9fbb05c731caf23b0fd58f3fbc1bb87a",
        "instanceId": 15329,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190436727,
                "type": ""
            }
        ],
        "screenShotFile": "001300c4-004f-0088-00a0-00f1001b0009.png",
        "timestamp": 1540190438828,
        "duration": 21
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9fbb05c731caf23b0fd58f3fbc1bb87a",
        "instanceId": 15329,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540190444186,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190444363,
                "type": ""
            }
        ],
        "screenShotFile": "0040008e-002b-0096-00e1-00fb007a005a.png",
        "timestamp": 1540190440827,
        "duration": 4877
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9fbb05c731caf23b0fd58f3fbc1bb87a",
        "instanceId": 15329,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00650097-0005-00fe-00bd-001700f00004.png",
        "timestamp": 1540190447513,
        "duration": 97
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b776733f17332a282a4b2072ac2d7521",
        "instanceId": 15386,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00530064-0016-00a0-0038-002d005f00c8.png",
        "timestamp": 1540190486567,
        "duration": 5649
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b776733f17332a282a4b2072ac2d7521",
        "instanceId": 15386,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190492384,
                "type": ""
            }
        ],
        "screenShotFile": "00db0054-00d2-0028-0037-00db0097002b.png",
        "timestamp": 1540190494362,
        "duration": 14
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b776733f17332a282a4b2072ac2d7521",
        "instanceId": 15386,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540190496450,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190496605,
                "type": ""
            }
        ],
        "screenShotFile": "001900cb-005a-00df-003d-0025001900ae.png",
        "timestamp": 1540190495989,
        "duration": 1629
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "b776733f17332a282a4b2072ac2d7521",
        "instanceId": 15386,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "005b009f-004c-0047-00bd-00a600af00d2.png",
        "timestamp": 1540190499293,
        "duration": 85
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4a2a507ec76a2bba4d1566bc6bf38df4",
        "instanceId": 15452,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00fd009e-0034-0014-0056-00cb001c0055.png",
        "timestamp": 1540190550231,
        "duration": 4143
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4a2a507ec76a2bba4d1566bc6bf38df4",
        "instanceId": 15452,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190554555,
                "type": ""
            }
        ],
        "screenShotFile": "00bc00c9-0062-0042-00fb-002c00d600e9.png",
        "timestamp": 1540190556378,
        "duration": 26
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4a2a507ec76a2bba4d1566bc6bf38df4",
        "instanceId": 15452,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540190558448,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540190558597,
                "type": ""
            }
        ],
        "screenShotFile": "00ce00dd-0036-000a-006f-002f00790035.png",
        "timestamp": 1540190557994,
        "duration": 1567
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4a2a507ec76a2bba4d1566bc6bf38df4",
        "instanceId": 15452,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ff00ff-0068-0032-0036-003d00f100ef.png",
        "timestamp": 1540190561449,
        "duration": 87
    },
    {
        "description": "login user|Title Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "2f7e780e1045e76cdb12852b379418e9",
        "instanceId": 15517,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "007b0046-00db-00f3-0037-002e006400e7.png",
        "timestamp": 1540190666272,
        "duration": 1038
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "2f7e780e1045e76cdb12852b379418e9",
        "instanceId": 15517,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00e200d2-002f-0019-0083-00ee00380057.png",
        "timestamp": 1540190667889,
        "duration": 0
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "2f7e780e1045e76cdb12852b379418e9",
        "instanceId": 15517,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00c5003f-009c-00b9-0024-005900a3002d.png",
        "timestamp": 1540190667934,
        "duration": 0
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "2f7e780e1045e76cdb12852b379418e9",
        "instanceId": 15517,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00400061-0013-00e3-004e-00c500f10029.png",
        "timestamp": 1540190667987,
        "duration": 0
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "2f7e780e1045e76cdb12852b379418e9",
        "instanceId": 15517,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00b7001f-00f3-008f-00dc-00770041004d.png",
        "timestamp": 1540190668035,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests is not defined"
        ],
        "trace": [
            "ReferenceError: requests is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:12:7)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should login and create post via API\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:11:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:8:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:4:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "005d0085-00e4-0098-00b3-0027006300d3.png",
        "timestamp": 1540191473465,
        "duration": 227
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "008900d6-004f-0001-00a0-0015005600dc.png",
        "timestamp": 1540191474322,
        "duration": 3499
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191477956,
                "type": ""
            }
        ],
        "screenShotFile": "0082005f-0095-000c-0057-00b8001800ab.png",
        "timestamp": 1540191479551,
        "duration": 15
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540191481557,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191481851,
                "type": ""
            }
        ],
        "screenShotFile": "00ca00be-005e-009e-0083-00bd007e00ed.png",
        "timestamp": 1540191481209,
        "duration": 1760
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00f2004b-0044-0086-008c-00ab00450055.png",
        "timestamp": 1540191484634,
        "duration": 86
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Wait timed out after 10010ms"
        ],
        "trace": [
            "Error: Wait timed out after 10010ms\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2364:22\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: <anonymous wait>\n    at ControlFlow.wait (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2352:17)\n    at WebDriver.wait (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/webdriver.js:712:29)\n    at ProtractorBrowser.to.(anonymous function) [as wait] (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/protractor/built/browser.js:62:29)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:46:15)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\nFrom: Task: Run it(\"Should navigate to the post created via API\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:43:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:8:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:4:1)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/undefined - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191486572,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191495649,
                "type": ""
            }
        ],
        "screenShotFile": "004d001a-00c2-0032-00e9-007500c7006b.png",
        "timestamp": 1540191486410,
        "duration": 19251
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "c3e580e7c0b19ebb1591f34df402de31",
        "instanceId": 16204,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00510040-002d-0029-00f5-004f0058004b.png",
        "timestamp": 1540191506190,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4365334177fc036126a9d5757c21c395",
        "instanceId": 16341,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests is not defined"
        ],
        "trace": [
            "ReferenceError: requests is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:13:7)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should login and create post via API\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:12:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:8:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:4:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "00a300c7-003b-0009-0089-00ab009300e2.png",
        "timestamp": 1540191603023,
        "duration": 219
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4365334177fc036126a9d5757c21c395",
        "instanceId": 16341,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004e006c-0021-00bd-00dc-00ff00350044.png",
        "timestamp": 1540191603817,
        "duration": 4049
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4365334177fc036126a9d5757c21c395",
        "instanceId": 16341,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191608015,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/undefined - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191609768,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191618831,
                "type": ""
            }
        ],
        "screenShotFile": "00fc0093-0027-0012-0082-00e8006e0057.png",
        "timestamp": 1540191609601,
        "duration": 9223
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4365334177fc036126a9d5757c21c395",
        "instanceId": 16341,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ce0096-008c-0012-009c-008e00910049.png",
        "timestamp": 1540191619352,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "023a4893433ce39d22979c058f71ca86",
        "instanceId": 16377,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests is not defined"
        ],
        "trace": [
            "ReferenceError: requests is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:13:7)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should login and create post via API\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:12:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:8:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:4:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "00710023-000b-00b7-008b-00a5002500a4.png",
        "timestamp": 1540191623348,
        "duration": 221
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "023a4893433ce39d22979c058f71ca86",
        "instanceId": 16377,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00620023-0080-00ef-009d-002d00f90042.png",
        "timestamp": 1540191624108,
        "duration": 3481
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "023a4893433ce39d22979c058f71ca86",
        "instanceId": 16377,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191627728,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/undefined - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191629482,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191638543,
                "type": ""
            }
        ],
        "screenShotFile": "004d00d0-0083-0082-00f6-00c800c6002a.png",
        "timestamp": 1540191629318,
        "duration": 9208
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "023a4893433ce39d22979c058f71ca86",
        "instanceId": 16377,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "000100a6-00bd-0083-007f-00ac0009004b.png",
        "timestamp": 1540191639008,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4af2ae5e70f49265c55aca204f061d03",
        "instanceId": 16502,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: requests is not defined"
        ],
        "trace": [
            "ReferenceError: requests is not defined\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:11:7)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:90:15\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Should login and create post via API\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:10:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:9:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:5:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "005d004e-0053-0044-0047-00e90065003f.png",
        "timestamp": 1540191728822,
        "duration": 253
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "4af2ae5e70f49265c55aca204f061d03",
        "instanceId": 16502,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/undefined - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191729828,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191738972,
                "type": ""
            }
        ],
        "screenShotFile": "002a0025-0036-0076-0069-00b400e2008c.png",
        "timestamp": 1540191729533,
        "duration": 9432
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "4af2ae5e70f49265c55aca204f061d03",
        "instanceId": 16502,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00ea0079-00de-0063-003b-00a400760094.png",
        "timestamp": 1540191739513,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "36b83ef26f3d916ed57b51baa98c9b9b",
        "instanceId": 16626,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "0055005f-007f-0092-00b9-009000b3008f.png",
        "timestamp": 1540191811824,
        "duration": 1101
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "36b83ef26f3d916ed57b51baa98c9b9b",
        "instanceId": 16626,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00a800a9-00b8-0017-00f0-00d1009800c0.png",
        "timestamp": 1540191813397,
        "duration": 3109
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "36b83ef26f3d916ed57b51baa98c9b9b",
        "instanceId": 16626,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191816656,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/414/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540191827848,
                "type": ""
            }
        ],
        "screenShotFile": "00160037-00bd-0012-00cf-00fe00540082.png",
        "timestamp": 1540191818452,
        "duration": 9388
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "36b83ef26f3d916ed57b51baa98c9b9b",
        "instanceId": 16626,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "003d007f-00f0-008a-0067-005c006c0099.png",
        "timestamp": 1540191828894,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "001c009d-002a-00d6-009b-00d200f5001a.png",
        "timestamp": 1540192806507,
        "duration": 1320
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ec0075-00cd-00a2-0003-0051002f001b.png",
        "timestamp": 1540192808268,
        "duration": 5208
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192813627,
                "type": ""
            }
        ],
        "screenShotFile": "0031001c-00a6-00bb-003d-00740067002a.png",
        "timestamp": 1540192815208,
        "duration": 13
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540192817278,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192817427,
                "type": ""
            }
        ],
        "screenShotFile": "008500c9-009e-00bd-009c-002800f600a5.png",
        "timestamp": 1540192816809,
        "duration": 1687
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "009200f8-007c-0098-006b-00c9001b0043.png",
        "timestamp": 1540192820116,
        "duration": 86
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00e5002e-00a8-0048-002b-003e008300b1.png",
        "timestamp": 1540192821800,
        "duration": 741
    },
    {
        "description": "Should verify if the post title is correct|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Failed: Cannot read property 'isPresent' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'isPresent' of undefined\n    at ProtractorExpectedConditions.presenceOf (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/protractor/built/expectedConditions.js:350:30)\n    at ProtractorExpectedConditions.visibilityOf (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/protractor/built/expectedConditions.js:390:30)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:52:23)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:94:23\n    at new ManagedPromise (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1082:7)\n    at controlFlowExecute (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:80:18)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2820:25)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\nFrom: Task: Run it(\"Should verify if the post title is correct\") in control flow\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:79:14)\n    at attemptAsync (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1916:24)\n    at QueueRunner.run (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1871:9)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1898:16\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1842:9\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasminewd2/index.js:16:5\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:51:5)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:802:7)\n    at describe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3375:18)\n    at Suite.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:13:3)\n    at addSpecsToSuite (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:833:25)\n    at Env.fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:821:7)\n    at fdescribe (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:3383:18)\n    at Object.<anonymous> (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:7:1)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/415/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192822687,
                "type": ""
            }
        ],
        "screenShotFile": "00eb00db-0009-000c-000c-001b00ad0065.png",
        "timestamp": 1540192824278,
        "duration": 4
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "d6d4c894c5b5112bb61535c885a908db",
        "instanceId": 17269,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00db003c-001c-00d4-0064-00f200a600cb.png",
        "timestamp": 1540192825885,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "007c00aa-0043-0069-00c5-00a900f20089.png",
        "timestamp": 1540192882218,
        "duration": 1039
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00bc00da-0081-00a8-00ba-006100f50066.png",
        "timestamp": 1540192883826,
        "duration": 3597
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192887563,
                "type": ""
            }
        ],
        "screenShotFile": "0033000f-0003-00d5-00d4-000500b700d9.png",
        "timestamp": 1540192889147,
        "duration": 14
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540192891179,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192891316,
                "type": ""
            }
        ],
        "screenShotFile": "00290000-0096-00fb-002c-00f900b800fb.png",
        "timestamp": 1540192890740,
        "duration": 1546
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "009e00db-0098-00be-0033-005400a1002f.png",
        "timestamp": 1540192894057,
        "duration": 88
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "006e00e6-007e-00ca-00a9-000e00c60076.png",
        "timestamp": 1540192895746,
        "duration": 384
    },
    {
        "description": "Should verify if the post title is correct|Successful login test|Log In Tests",
        "passed": false,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": [
            "Expected 'Async test' to equal undefined."
        ],
        "trace": [
            "Error: Failed expectation\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/Protractor/tests/loginTests.js:53:46\n    at ManagedPromise.invokeCallback_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:1379:14)\n    at TaskQueue.execute_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2913:14)\n    at TaskQueue.executeNext_ (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2896:21)\n    at asyncRun (/Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:2775:27)\n    at /Users/marius.bodea/Documents/MyWork/Accenture/ProtractorMocha/node_modules/selenium-webdriver/lib/promise.js:639:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/416/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192896266,
                "type": ""
            }
        ],
        "screenShotFile": "0026001e-000d-00ba-0044-00ed00ed007c.png",
        "timestamp": 1540192897862,
        "duration": 77
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "32412eb230372dd1cdbe000c7980ece2",
        "instanceId": 17319,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "0003004c-00a1-00ee-00ca-00790043002d.png",
        "timestamp": 1540192899523,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00560033-004c-001e-0069-003b004600ed.png",
        "timestamp": 1540192978333,
        "duration": 1037
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "004f009a-0003-008d-00b0-00c900550057.png",
        "timestamp": 1540192979942,
        "duration": 4701
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192984778,
                "type": ""
            }
        ],
        "screenShotFile": "003600f7-0046-0058-0066-000600c500ad.png",
        "timestamp": 1540192986358,
        "duration": 14
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540192988278,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192988567,
                "type": ""
            }
        ],
        "screenShotFile": "000a00ca-001b-00de-0001-00a300060047.png",
        "timestamp": 1540192987953,
        "duration": 1682
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00bb002c-0076-00a3-004d-005c009700c9.png",
        "timestamp": 1540192991243,
        "duration": 80
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00250056-00e2-0030-00ca-00920080001b.png",
        "timestamp": 1540192992910,
        "duration": 370
    },
    {
        "description": "Should verify if the post title is correct|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/417/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540192993421,
                "type": ""
            }
        ],
        "screenShotFile": "004200a4-00e0-00f3-0046-007500d30099.png",
        "timestamp": 1540192995016,
        "duration": 76
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "a0abffeed8766240ded4a876360cc847",
        "instanceId": 17387,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "007500a9-00ff-0096-0051-000d008b004f.png",
        "timestamp": 1540192996748,
        "duration": 0
    },
    {
        "description": "Should login and create post via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00670022-00ee-0005-00b4-002b00ec0059.png",
        "timestamp": 1540194495144,
        "duration": 1047
    },
    {
        "description": "Should open the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "005900b7-00f9-00c1-00d9-0069004d00c4.png",
        "timestamp": 1540194496762,
        "duration": 5060
    },
    {
        "description": "Should verify the title of the site|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540194501965,
                "type": ""
            }
        ],
        "screenShotFile": "001c00d4-00ff-00f1-001b-00b500bd0040.png",
        "timestamp": 1540194503658,
        "duration": 19
    },
    {
        "description": "Should log in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/login - This page includes a password or credit card input in a non-secure context. A warning has been added to the URL bar. For more information, see https://goo.gl/zmWq3m.",
                "timestamp": 1540194505801,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540194505949,
                "type": ""
            }
        ],
        "screenShotFile": "0011001d-0019-0060-00b3-004b00dd0061.png",
        "timestamp": 1540194505330,
        "duration": 2081
    },
    {
        "description": "Should verify if the user is logged in|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "000c009e-002a-003d-0047-0006009200ed.png",
        "timestamp": 1540194509100,
        "duration": 81
    },
    {
        "description": "Should navigate to the post created via API|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "00ef00ca-0089-0044-0040-007a008f0093.png",
        "timestamp": 1540194510824,
        "duration": 384
    },
    {
        "description": "Should verify if the post title is correct|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/418/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540194511353,
                "type": ""
            }
        ],
        "screenShotFile": "00030016-009a-00d4-0078-003e00b10052.png",
        "timestamp": 1540194513075,
        "duration": 73
    },
    {
        "description": "Should verify if the post description is correct|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00f50044-00b7-00e7-0097-003e00670076.png",
        "timestamp": 1540194514828,
        "duration": 77
    },
    {
        "description": "Should click the edit button|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [],
        "screenShotFile": "007700fc-0067-0096-007c-003b005500bd.png",
        "timestamp": 1540194516622,
        "duration": 379
    },
    {
        "description": "Should edit the title of the post|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://bodeamariuscosmin.pythonanywhere.com/mlot/post/418/edit/static/images/favicon-16.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1540194517145,
                "type": ""
            }
        ],
        "screenShotFile": "00a100fd-00c6-002d-00ce-00d600b20045.png",
        "timestamp": 1540194518831,
        "duration": 728
    },
    {
        "description": "Should verify if the edited post title is correct|Successful login test|Log In Tests",
        "passed": true,
        "pending": false,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00110050-001a-00af-0020-001000c100c2.png",
        "timestamp": 1540194521393,
        "duration": 134
    },
    {
        "description": "login user|Title Tests",
        "passed": false,
        "pending": true,
        "os": "Mac OS X",
        "sessionId": "9a593d10960fcec855ac732ea58f989e",
        "instanceId": 18272,
        "browser": {
            "name": "chrome",
            "version": "69.0.3497.100"
        },
        "message": "Pending",
        "browserLogs": [],
        "screenShotFile": "00bf008e-00e4-00de-00cd-004500e7008c.png",
        "timestamp": 1540194522275,
        "duration": 0
    }
];

    this.sortSpecs = function () {
        this.results = results.sort(function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) return -1;else if (a.sessionId > b.sessionId) return 1;

    if (a.timestamp < b.timestamp) return -1;else if (a.timestamp > b.timestamp) return 1;

    return 0;
});
    };

    this.sortSpecs();
});

app.filter('bySearchSettings', function () {
    return function (items, searchSettings) {
        var filtered = [];
        var prevItem = null;

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.displaySpecName = false;

            countLogMessages(item);

            var hasLog = searchSettings.withLog && item.browserLogs && item.browserLogs.length > 0;
            if (searchSettings.description === '' ||
                (item.description && item.description.toLowerCase().indexOf(searchSettings.description.toLowerCase()) > -1)) {

                if (searchSettings.passed && item.passed || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                } else if (searchSettings.failed && !item.passed && !item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                } else if (searchSettings.pending && item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                }

            }
        }

        return filtered;
    };
});

var isValueAnArray = function (val) {
    return Array.isArray(val);
};

var checkIfShouldDisplaySpecName = function (prevItem, item) {
    if (!prevItem) {
        item.displaySpecName = true;
        return;
    }

    if (getSpec(item.description) != getSpec(prevItem.description)) {
        item.displaySpecName = true;
        return;
    }
};

var getSpec = function (str) {
    var describes = str.split('|');
    return describes[describes.length - 1];
};

var countLogMessages = function (item) {
    if ((!item.logWarnings || !item.logErrors) && item.browserLogs && item.browserLogs.length > 0) {
        item.logWarnings = 0;
        item.logErrors = 0;
        for (var logNumber = 0; logNumber < item.browserLogs.length; logNumber++) {
            var logEntry = item.browserLogs[logNumber];
            if (logEntry.level === 'SEVERE') {
                item.logErrors++;
            }
            if (logEntry.level === 'WARNING') {
                item.logWarnings++;
            }
        }
    }
};
