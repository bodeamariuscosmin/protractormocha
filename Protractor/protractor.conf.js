// var HtmlReporter = require('protractor-jasmine2-screenshot-reporter'),
var HtmlReporter = require('protractor-beautiful-reporter'),
  oldDate = new Date(),
  date = (oldDate.getMonth()+1) + "-" + oldDate.getDate() + "-" + oldDate.getFullYear() + " " + oldDate.getHours() + "-" + oldDate.getMinutes();

exports.config = {
  framework: 'jasmine2',
  baseUrl: "http://bodeamariuscosmin.pythonanywhere.com/mlot",
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect: true,

  // Suites or specs of tests
  specs: [
          'tests/testsWithMocha.js',
          'tests/loginTests.js',
          ],
  suites: {
    mocha: 'tests/testsWithMocha.js',
    protractor: 'tests/loginTests.js'},

  // For parallel execution
 //  multiCapabilities: [{
 //    'browserName': 'firefox'
	// }, {
 //    'browserName': 'chrome'
	// }],

  // For single browser execution
	capabilities: {
    'browserName': 'chrome'
	},

  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report
    isVerbose: true,
    includeStackTrace: true
  },

  onPrepare: function() {
    //browser.driver.manage().window().maximize();

    // HTML Reporter
    // jasmine.getEnv().addReporter(new HtmlReporter({
    //   dest: 'ProtractorReportsNaspa/' + date + '/',
    //   filename: 'TestReport.html'
    // }));

      // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
      jasmine.getEnv().addReporter(new HtmlReporter({
         baseDirectory: 'Protractor/ProtractorReports'
      }).getJasmine2Reporter());

    browser.ignoreSynchronization = true;
    EC = protractor.ExpectedConditions;
  },

  onComplete: function() {
    browser.close();
  }
}