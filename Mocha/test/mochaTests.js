var expect = require("chai").expect,
    requestsFactory = require("../functions/requestsFactory.js");

describe('My List Of Tasks Tests', function() {
    var request = new requestsFactory();
    var response, body;

    it("GET all posts", function(done) {
        response = request.getAllPosts();
        body = response;
        console.log(body);
        done();
    });

    it('GET post by index', function(done) {
        response = request.getPostByIndex(216);
        body = response[0];
        console.log(body);
        expect(body.author).to.equal(2);
        done();
    });

    it('Log in test', function(done) {
        response = request.loginUser('testaccount', 'password123');
        body = response;
        console.log(body);
        expect(body.username).to.equal('testaccount');
        done();
    });

    it('Log in and create post test', function(done) {
        response = request.loginUser('testaccount', 'password123');
        body = response;
        console.log(body);
        expect(body.username).to.equal('testaccount');

        response = request.createPost('Async test', 'This is a Mocha test made using sync-request library!');
        body = response[0];
        console.log(body);
        expect(body.title).to.equal('Async test');
        done();
    });
});




