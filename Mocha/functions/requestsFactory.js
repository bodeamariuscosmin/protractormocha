request = require("sync-request"),
mochaExpect = require("chai").expect;
        
var RequestsFactory = function (){

    var that = this;

    this.getAllPosts = function()
    {
        // Set the headers
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        // Configure the request
		var options = {
            url: 'http://bodeamariuscosmin.pythonanywhere.com/mlot/api/posts',
            method: 'GET',
            headers: headers,
            json: true
        };

        var res = request('GET', options.url);
        mochaExpect(res.statusCode).to.equal(200);
        return JSON.parse(res.getBody().toString());
    }

    this.getPostByIndex = function(index, done)
    {
        // Set the headers
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        // Configure the request
		var options = {
            url: 'http://bodeamariuscosmin.pythonanywhere.com/mlot/api/post/' + index + '/',
            method: 'GET',
            headers: headers,
            json: true
        };

        var res = request('GET', options.url);
        mochaExpect(res.statusCode).to.equal(200);
        return JSON.parse(res.getBody().toString());
    };
  
    this.loginUser = function(username, password, done)
    {
        // Set the headers
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        // Configure the request
        var options = {
            url: 'http://bodeamariuscosmin.pythonanywhere.com/mlot/api/login',
            method: 'POST',
            headers: headers,
            json: true,
            body: {'username': username, 'password': password}
        };

        // Start the request
        var res = request('POST', options.url, {json: options.body});
        mochaExpect(res.statusCode).to.equal(200);
        return JSON.parse(res.getBody().toString());
    };

    this.createPost = function(title, description, done)
    {
        // Set the headers
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        // Configure the request
		var options = {
            url: 'http://bodeamariuscosmin.pythonanywhere.com/mlot/api/post/new/',
            method: 'POST',
            headers: headers,
            json: true,
            body: {'author': 22, 'title': title, 'text': description}
        };

        // Start the request
        var res = request('POST', options.url, {json: options.body});
        mochaExpect(res.statusCode).to.equal(200);
        return JSON.parse(res.getBody().toString());
    };
  }
  
  module.exports = RequestsFactory;